var basic_fields = {
	// Wistron Tag
	id_1 : {
		"type":"string",
		"maxLength":13,
		"isRequired":true,
		"message":null
	},
	// RIM tag
	id_2 : {
		"type":"string",
		"maxLength":30,
		"isRequired":false,
		"message":null
	},
	// Serial Number
	id_3 : {
		"type":"string",
		"maxLength":50,
		"isRequired":true,
		"message":null
		
	},
	// model
	id_5: {
		"type":"string",
		"maxLength":255,
		"isRequired":true,
		"message":null
	},
	// manufacture
	id_6: {
		"type":"string",
		"maxLength":255,
		"isRequired":true,
		"message":null
	},
	// description
	id_7: {
		"type":"text",
		"maxLength":0,
		"isRequired":true,
		"message":null
	},
	// date received
	id_8: {
		"type":"date",
		"maxLength":255,
		"isRequired":false,
		"message":null
	},
	// Cost center
	id_9: {
		"type":"string",
		"maxLength":13,
		"isRequired":true,
		"message":null
	},
	// Purchase request order number
	id_10: {
		"type":"string",
		"maxLength":15,
		"isRequired":true,
		"message":null
	},
	// Purchase order number
	id_11: {
		"type":"string",
		"maxLength":15,
		"isRequired":true,
		"message":null
	},
	// expected life
	id_12: {
		"type":"number",
		"maxLength":null,
		"isRequired":false,
		"message":null

	},
	
	// current assigned user
	id_13: {
		"type":"select",
		"maxLength":null,
		"isRequired":true,
		"message":null
	},
	// current assigned user
	id_14: {
		"type":"select",
		"maxLength":null,
		"isRequired":true,
		"message":null
	},
	// current owner
	id_15: {
		"type":"select",
		"maxLength":null,
		"isRequired":true,
		"message":null

	},
	// current location
	id_16: {
		"type":"select",
		"maxLength":null,
		"isRequired":true,
		"message":null
	},
	// comments
	id_17: {
		"type":"text",
		"maxLength":null,
		"isRequired":false,
		"message":null
	}
	
	
};
var license_fields = {
	// Hardware start date
	id_20: {
		"type":"date",
		"maxLength":null,
		"isRequired":false,
		"message":"HW start date cannot be later than end date"
	},
	// Hardware end date
	id_21: {
		"type":"date",
		"maxLength":null,
		"isRequired":false,
		"message":"HW end date cannot be earlier than start date"
	},
	// HW cost
	id_22: {
		"type":"number",
		"maxLength":null,
		"isRequired":false,
		"message":"Cost must be number only"
	},
	// Software start date
	id_23: {
		"type":"date",
		"maxLength":null,
		"isRequired":false,
		"message":"SW start date cannot be later than end date"
	},
	// Software end date
	id_24: {
		"type":"date",
		"maxLength":null,
		"isRequired":false,
		"message":"SW end date cannot be earlier than start date"
	},
	// SW cost
	id_25: {
		"type":"number",
		"maxLength":null,
		"isRequired":false,
		"message":"SW cost must be number only"
	},
	// license comments
	id_26: {
		"type":"text",
		"maxLength":null,
		"isRequired":false,
		"message":null
	}
}
var calibration_fields = {
	// calibration reference interval
	id_31: {
		"type":"number",
		"maxLength":2,
		"isRequired":false,
		"message":null
	},
	// last calibration date
	id_32: {
		"type":"date",
		"maxLength":null,
		"isRequired":false,
		"message":null
	},
	// next calibration date (MFG)
	id_33: {
		"type":"date",
		"maxLength":null,
		"isRequired":false,
		"message":null
	},
	// next calibration date (WCH)
	id_34: {
		"type":"date",
		"maxLength":null,
		"isRequired":false,
		"message":null
	},
	// calibration interval
	id_35: {
		"type":"number",
		"maxLength":2,
		"isRequired":false,
		"message":null
	},
	// calibration cost
	id_36: {
		"type":"number",
		"maxLength":5,
		"isRequired":false,
		"message":null
	},
	// calibration end date
	id_37: {
		"type":"date",
		"maxLength":null,
		"isRequired":false,
		"message":null
	},
	// classification
	id_38: {
		"type":"string",
		"maxLength":255,
		"isRequired":false,
		"message":null
	},
	// comment
	id_39: {
		"type":"text",
		"maxLength":null,
		"isRequired":false,
		"message":null
	}
}
function cleanupMarks(modal_window){
	modal_window.getElement().find('.has-error, .has-success, .has-feedback ').removeClass("has-error has-success has-feedback");
	modal_window.getElement().find('small').remove();
	modal_window.getElement().find('.glyphicon-ok, .glyphicon-remove').remove();
}
function validate(modal_window, fields){
	var failedCount = 0;
	//cleanupMarks(modal_window);
	for(var key in fields){
		console.log(key);
		var option = fields[key];
		var target = modal_window.getElement().find('#'+key);
		var value = target.val();
		if($(target).is("textarea")){
			//console.log(key);
		}
		if(option.isRequired){
			if(option.type == "select" || option.type == 'date'){
				if($(target)[0].selectedIndex == 0){
					$(target).parent().addClass("has-error ");
					$(target).parent().append('<small class="help-block col-lg-offset-3 col-lg-9" style="">This field is required and cannot be empty</small>');
					failedCount++;
					continue;
				}
			}
			if(value.length == 0){
				$(target).parent().addClass("has-error has-feedback");
				$(target).parent().append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
				$(target).parent().css("clear","both").append('<small class="help-block col-lg-offset-3 " style="">This field is required and cannot be empty</small>');
				failedCount++;
				continue;
			}
			
		}
		
		switch(option.type){
			case "string":
				if(value != ""){
					if(containIllegalCharacters(value)){
						$(target).parent().addClass("has-error ");
						$(target).parent().append('<small class="help-block col-lg-offset-3 col-lg-9" style="">Cannot have illegal characters \' and \"</small>');
					}else if(option.maxLength != null){
						if(value.length > option.maxLength){
							$(target).parent().addClass("has-error ");
							$(target).parent().append('<small class="help-block col-lg-offset-3 col-lg-9" style="">The maximum length for this field is '+option.maxLength+'</small>');
							continue;
						}
					}
				}
				break;
			case "select":
				//console.log("Select index : "+ $(target)[0].selectedIndex);
				break;
			case "number":
				if(value != ""){
					if(!isNumbers(value)){
						$(target).parent().addClass("has-error ");
						$(target).parent().append('<small class="help-block col-lg-offset-3 col-lg-9" style="">Cannot have illegal characters \' and \"</small>');
						continue;
					}
				}
				break;
			case "text":
			break;
		}
		if(value != ""){
			if(option.type != 'select' && option.type != 'date'){
				$(target).parent().addClass("has-success has-feedback");
				$(target).parent().append('<span class="glyphicon glyphicon-ok form-control-feedback"></span>');
			}else{
				$(target).parent().addClass("has-success");
				
			}
		}
	}

	return failedCount;
}
function isNumbers(value){
	return /^\d+\.*\d{0,2}$/.test(value);
}
function containIllegalCharacters(value){
	return /(\'|\")/.test(value);
}
function bindValidation(modal_window){
	
}
