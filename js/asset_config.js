/*!
 * Asset Management v1.1
 * 
 * The system is designed and developed by Zachary Huang 
 * If you have any issue, please contact me at zachary1104@gmail.com
 * You are welcome to use but please keep the copyright information.
 *	
 * I designed this system in my personal private time and this system specific design for the Operation Team use. 
 *
 */


/**
 *	Window manager instance
 *	@Type : object
 */
var oWM = null;


var oTabControllers = {
	"oMainTabController" : null,
	"oAssetInfoTabController":null
}
var oDataSource ={
	/**
	 *	Account List 
	 *	@Type : array
	 *
	 */
	"aAccountList":[],
	/**
	 *	Location List 
	 *	@Type : array
	 *
	 */
	"aLocationList":[],
	/**
	 *	Status List
	 *	@Type : array
	 *
	 */
	"aStatus":[],
	/**
	 *	WCH team List
	 *	@Type : array
	 *
	 */
	"aTeamList":[]
}
var oGrids = {
	"asset_table" : null,
	"account_table" : null
};
var oActiveWindows = null;

var oSettingWindows = {
	
	"oMainWindow" : null,
	"oChildWindow" : null
}
var oMessageWindow = null;
	/**
	 *	Current selected row object
	 */
var oSelectedRow = null;

var oAttachmentList = {
	/**
	 *	Asset attachment file list
	 *	@Type	: array
	 */
	"aAssetFileList" : [],
	/**
	 *	Asset calibration attachment file list
	 *	@Type	: array
	 */
	"aCalibrationFileList" : []
}
var iIsLogin = false;

$( document ).ready(function(){

	
	
	oWM = new WindowManager({
        container: "#windowPane",
        windowTemplate: $('#window_template').html()
    });
	displayProcessing(null);
	// initial main window tabs control
	oTabControllers.oMainTabController = $('#tab_control a:last').tab('show');
	
	loadDataSource_AccountList();
	loadDataSource_LocationList();
	loadDataSource_status();
	loadDataSource_teamList();
	
	
	//initial tables
	initial_asset_table();
	initial_account_table();
	//setTimeout(function(){oProcessing.modal("hide")},500);
	
	$("#toggle-icon").on("click",function(){
		if($(".main_tab_control ul").hasClass("nav-close")){
			
			$(".main_tab_control ul").removeClass("nav-close");		
			$("#toggle-icon > i").addClass("fa-caret-square-o-left");
			$("#toggle-icon > i").removeClass("fa-caret-square-o-right");
			setTimeout(function(){
				if(!$(".main_tab_control ul").hasClass("nav-close")){
					$(".main_tab_control ul").addClass("nav-close");
					$("#toggle-icon > i").removeClass("fa-caret-square-o-left");
					$("#toggle-icon > i").addClass("fa-caret-square-o-right");
					
				}
			}, 5000);
		}else{
			$(".main_tab_control ul").addClass("nav-close");
			$("#toggle-icon > i").removeClass("fa-caret-square-o-left");
			$("#toggle-icon > i").addClass("fa-caret-square-o-right");
			console.log($(".main_tab_control ul").find('.active'));
			
		
		}		
	});
	
	$("#setting-tab").on("click",function(){
		if(oSettingWindows.oMainWindow != null){
			return;
		}
		var liTag = this;
		oSettingWindows.oMainWindow = oWM.createWindow({
			title: "Setting",
		bodyContent: '<table id="setting-button-list"><tr><td><button type="button" id="btn-account" class="btn btn-primary " >Accounts</button></td></tr>'+
					 '<tr><td><button type="button" id="btn-location" class="btn btn-primary">Location</button></td></tr>'+
					 '<tr><td><button type="button" id="btn-status" class="btn btn-primary">Status</button></td></tr>'+
					 '<tr><td><button type="button" id="btn-team" class="btn btn-primary">Team information</button></td></tr></table>',
		footerContent: '<button type="button" class="btn btn-default" id="btn-cancel" data-dismiss="window">'+
					   '<span class="glyphicon glyphicon-home"></span>Close</button>'
					
		
		});
		
		$(oSettingWindows.oMainWindow.$el[0]).css("top","17%")
		oSettingWindows.oMainWindow.getElement().find('#btn-cancel').on("click",function(){
			$(".nav-tabs li:nth-child(3)").removeClass("active");
			$(".nav-tabs li:nth-child(2)").addClass("active");
			oSettingWindows.oMainWindow = null;
		});
		
		oSettingWindows.oMainWindow.getElement().find('#btn-account').on("click",function(){
			oSettingWindows.oChildWindow  = oWM.createWindow({
            title: "Account Setting",
            bodyContent:"<div id='account_table_wrapper'><table id='account_table' style='display:none'></table></div>",
            footerContent: '<button type="button" class="btn btn-default" data-dismiss="window">Close</button>'
			});
			
			$(oSettingWindows.oChildWindow.$el[0]).css({"top":"17%","left":"30%"});
			oSettingWindows.oMainWindow.setBlocker(oSettingWindows.oChildWindow);
			initial_account_table();
		});
		
		
		oSettingWindows.oMainWindow.getElement().find('#btn-location').on("click",function(){
			var options = "";
			var selected_id = "";
			for(var key in oDataSource.aLocationList){
				options += ("<option data-id='"+key+"'>" + oDataSource.aLocationList[key] + "</option>");
			}
			oSettingWindows.oChildWindow  = oWM.createWindow({
            title: "Location Setting",
            bodyContent:"<select id='setting-location' size='8'>"+options+"</select><br/>" +
			            "<button id='btn-add-location' type='button' class='btn btn-primary btn-sm' style='width:80px;margin-top:2%; margin-right:10px'>ADD</button><button id='btn-delete-location' type='button' class='btn btn-danger btn-sm' style='width:80px;margin-top:2%' disabled >Delete</button>",
            footerContent: '<button type="button" class="btn btn-default" data-dismiss="window">Close</button>'
			});
			$(oSettingWindows.oChildWindow.$el[0]).css({"top":"17%"});
			oSettingWindows.oMainWindow.setBlocker(oSettingWindows.oChildWindow);
			
			$(oSettingWindows.oChildWindow.getElement().find("#setting-location")).on("dblclick",function(){ 
				selected_id = $(this).find(":selected").attr("data-id"); 
				initialDataSourceEditor("Location Editor",false,$(this).val(),$(this).find(":selected").attr("data-id"),"Location");
			});
			$(oSettingWindows.oChildWindow.getElement().find("#setting-location")).on("click",function(){ 
				selected_id = $(this).find(":selected").attr("data-id");
				$(oSettingWindows.oChildWindow.getElement().find("#btn-delete-location").attr("disabled",false));
			});
			$(oSettingWindows.oChildWindow.getElement().find("#btn-add-location")).on("click",function(){initialDataSourceEditor("Location Editor",true,null,null,"Location");});
			$(oSettingWindows.oChildWindow.getElement().find("#btn-delete-location")).on("click",function(){
				$(oSettingWindows.oChildWindow.getElement().find("#btn-delete-location").attr("disabled",true));
				confirmToDelete(oSettingWindows.oChildWindow,"Locations",selected_id);
			});
			
		});
		oSettingWindows.oMainWindow.getElement().find('#btn-status').on("click",function(){
			var options = "";
			var selected_id = "";
			for(var key in oDataSource.aStatus){
				options += ("<option data-id='"+key+"'>" + oDataSource.aStatus[key] + "</option>");
			}
			oSettingWindows.oChildWindow  = oWM.createWindow({
            title: "Status Setting",
            bodyContent:"<select id='setting-status' size='8' style='width:200px'>"+options+"</select><br />"+
						"<button id='btn-add-status' type='button' class='btn btn-primary btn-sm' style='width:80px;margin-top:2%; margin-right:10px' >ADD</button><button id='btn-delete-status' type='button' class='btn btn-danger btn-sm' style='width:80px;margin-top:2%' disabled>Delete</button>",
            footerContent: '<button type="button" class="btn btn-default" data-dismiss="window">Close</button>'
			});
			$(oSettingWindows.oChildWindow.$el[0]).css({"top":"17%"});
			oSettingWindows.oMainWindow.setBlocker(oSettingWindows.oChildWindow);
			$(oSettingWindows.oChildWindow.getElement().find("#setting-status")).on("dblclick",function(){
				selected_id = $(this).find(":selected").attr("data-id"); 
				initialDataSourceEditor("Status Editor",false,$(this).val(),selected_id,"Status"); 
			});
			$(oSettingWindows.oChildWindow.getElement().find("#setting-status")).on("click",function(){ 
				selected_id = $(this).find(":selected").attr("data-id");
				$(oSettingWindows.oChildWindow.getElement().find("#btn-delete-status").attr("disabled",false));
			});
			$(oSettingWindows.oChildWindow.getElement().find("#btn-add-status")).on("click",function(){initialDataSourceEditor("Status Editor",true,null,null,"Status");});
			$(oSettingWindows.oChildWindow.getElement().find("#btn-delete-status")).on("click",function(){
				confirmToDelete(oSettingWindows.oChildWindow,"Status",selected_id);
			});
		});
		oSettingWindows.oMainWindow.getElement().find('#btn-team').on("click",function(){
			var options = "";
			var selected_id = "";
			for(var key in oDataSource.aTeamList){
				options += ("<option data-id='"+key+"'>" + oDataSource.aTeamList[key] + "</option>");
			}
			oSettingWindows.oChildWindow  = oWM.createWindow({
            title: "Team Setting",
            bodyContent:"<select id='setting-team' size='8' style='width:200px'>"+options+"</select><br />" +
			            "<button id='btn-add-team' type='button' class='btn btn-primary btn-sm' style='width:80px;margin-top:2%; margin-right:10px'>ADD</button><button id='btn-delete-team' type='button' class='btn btn-danger btn-sm' style='width:80px;margin-top:2%' disabled>Delete</button>",
            footerContent: '<button type="button" class="btn btn-default" data-dismiss="window">Close</button>'
			});
			$(oSettingWindows.oChildWindow.$el[0]).css({"top":"17%"});
			oSettingWindows.oMainWindow.setBlocker(oSettingWindows.oChildWindow);
			
			$(oSettingWindows.oChildWindow.getElement().find("#setting-team")).on("dblclick",function(){
				selected_id = $(this).find(":selected").attr("data-id");
				initialDataSourceEditor("Team Editor",false,$(this).val(),selected_id,"Team");	
			});
			$(oSettingWindows.oChildWindow.getElement().find("#setting-team")).on("click",function(){ 
				selected_id = $(this).find(":selected").attr("data-id");
				$(oSettingWindows.oChildWindow.getElement().find("#btn-delete-team").attr("disabled",false));
			});
			$(oSettingWindows.oChildWindow.getElement().find("#btn-add-team")).on("click",function(){initialDataSourceEditor("Team Editor",true,null,null,"Team");});
			$(oSettingWindows.oChildWindow.getElement().find("#btn-delete-team")).on("click",function(){confirmToDelete(oSettingWindows.oChildWindow,"Team",selected_id);});
		});
	});
	
});
function initialDataSourceEditor(title, newEntry, value, index, type){
	var userList = "";
	if(type == "Location"){
		userList = "<label class='control-label for='datasource-entry'>Manager</label><select id='user-list' class='form-control'><option>--User--</option>";
		for(var key in oDataSource.aAccountList){
			var val = oDataSource.aAccountList[key];
			userList += "<option data-id="+key+">"+val+"</option>";
		}
		userList += "</select>";
	}
	var child = oWM.createWindow({
        title: title,
        bodyContent:'<div class="form-group"><label class="control-label" for="datasource-entry">'+type+
					'</label><input id="datasource-entry" class="form-control input-sm" type="text" data-type="integer" value="'+((value != null)?value:"")+'"></div>'+
					userList,
        footerContent:'<button id="btn-submit" type="button" class="btn btn-primary">Submit</button>'+
					  '<button id="btn-close" type="button" class="btn btn-default" data-dismiss="window">Close</button>'
    });
	
	if(newEntry == false && type == "Location"){
		var location_name = ajaxCall("getLocationManager",JSON.stringify({"location_id":index}));
		if(location_name != null){
			$(child.getElement().find('#user-list')).val(location_name);
		}else{
			$(child.getElement().find('#user-list')).val("--User--");
		}
	}
	$(child.$el[0]).css("top","18%")
	oSettingWindows.oChildWindow.setBlocker(child);
	child.getElement().find('#btn-submit').on("click",function(){
		displayProcessing(oSettingWindows.oChildWindow);
		
		if(type == "Location"){
			var temp = child.getElement().find('#user-list').find(":selected").attr("data-id");
			if(newEntry == false){
				var aJson = {};
				aJson[0] = index;
				aJson[1] = child.getElement().find('#datasource-entry').val();
				aJson[2] = (typeof  temp === "undefined")?"NULL":temp;
				console.log(aJson);
				ajaxCall("updateLocation",JSON.stringify(aJson));
			}else{
				var aJson = {};
				aJson[0] = child.getElement().find('#datasource-entry').val();
				aJson[1] = (typeof  temp === "undefined")?"NULL":temp;
				ajaxCall("insertLocation",JSON.stringify(aJson));
			}
		}else if(type == "Status"){
			if(newEntry == false){
				var aJson = {};
				aJson[0] = index;
				aJson[1] = child.getElement().find('#datasource-entry').val();
				ajaxCall("updateStatus",JSON.stringify(aJson));
			}else{
				var aJson = {};
				aJson[0] = child.getElement().find('#datasource-entry').val();
				ajaxCall("insertStatus",JSON.stringify(aJson));
			}
		}else if(type == "Team"){
			if(newEntry == false){
				var aJson = {};
				aJson[0] = index;
				aJson[1] = child.getElement().find('#datasource-entry').val();
				ajaxCall("updateTeam",JSON.stringify(aJson));
			}else{
				var aJson = {};
				aJson[0] = child.getElement().find('#datasource-entry').val();
				ajaxCall("insertTeam",JSON.stringify(aJson));
			}
		}
	});
}
function initial_asset_table(){
	oGrids.asset_table = $("#asset_table").flexigrid({
	url: 'php/AssetList.php',
	errormsg: "Unable to connect to the DB server",
	dataType: 'json',
	colModel : [
		{display: 'ID', name : 'id', width : 50, sortable : true, align: 'left'},
		{display: 'Wistron Tag', name : 'wistron_tag', width : 120, sortable : true, align: 'left'},
		{display: 'RIM Tag', name : 'rim_tag', width : 120, sortable : true, align: 'left'},
		{display: 'Serial Number', name : 'serial_number', width : 200, sortable : true, align: 'center'},
		{display: 'Status', name : 'status', width : 100, sortable : true, align: 'center'},
		{display: 'Model', name : 'model', width : 200, sortable : true, align: 'center'},
		{display: 'Manufacturer', name : 'manufacturer', width : 200, sortable : true, align: 'center'},
		{display: 'Description', name : 'description', width : 300, sortable : true, align: 'center'},
		{display: 'Date Received', name : 'received_date', width : 150, sortable : true, align: 'center'},
		{display: 'Cost Center', name : 'cost_center', width : 120, sortable : true, align: 'center'},
		{display: 'P.O. Request No', name : 'po_request_no', width : 150, sortable : true, align: 'center'},
		{display: 'P.O. Order No', name : 'po_order_no', width : 150, sortable : true, align: 'center'},
		{display: 'Expected Life', name : 'expected_life', width : 120, sortable : true, align: 'center'},
		{display: 'Current User', name : 'current_assigned_user', width : 150, sortable : true, align: 'center'},
		{display: 'Team Belong To', name : 'team', width : 150, sortable : true, align: 'center'},
		{display: 'Owner', name : 'current_owner', width : 150, sortable : true, align: 'center'},
		{display: 'Location', name : 'location', width : 350, sortable : true, align: 'left'},
		{display: 'Comments', name : 'asset_comments', width : 300, sortable : true, align: 'center'},
		{display: 'Calibration Required', name : 'calibration_required', width : 170, sortable : true, align: 'center'},
		{display: 'Maintenance Required', name : 'maintenance_required', width : 170, sortable : true, align: 'center'}
		],
	buttons:[   
        {name:'ADD',bclass:'add',onpress:new_asset},
		{separator:true},
		{name:'FILTER',bclass:'btn-filter',onpress:custom_filter},	 
		{separator:true},
		{name:'SEARCH',bclass:'btn-search',onpress:custom_search},
    	{separator:true},
		{name:'RESET',bclass:'btn-reset',onpress:reset_filters},
		{separator:true},		
		{name:'EXPORT',bclass:'btn-export',onpress:custom_export},
		{separator:true},
		],   
	singleSelect: true,
	sortname: "id",
	sortorder: "asc",
	usepager: true,
	title: 'Asset Table',
	useRp: true,
	rp: 30,
	showTableToggllinueBtn: true,
	width: 'auto',
	height: ($(window).height() * 0.8923) * 0.80,
	resizable:false,
	blockOpacity:0.5,
	onDoubleClick:rowDbclick
	
	});
	
}

function initial_account_table(){
	oGrids.account_table = $("#account_table").flexigrid({
	url: 'php/AccountList.php',
	errormsg: "Unable to connect to the DB server",
	dataType: 'json',
	colModel : [
		{display: 'Employee ID', name : 'id', width : 150, sortable : true, align: 'left'},
		{display: 'Full Name', name : 'full_name', width : 250, sortable : true, align: 'left'},
		{display: 'Email', name : 'email', width : 350, sortable : true, align: 'left'},
		{display: 'Privilege', name : 'privilege', width : 100, sortable : true, align: 'center'},
		{display: 'Is Online', name : 'online', width : 110, sortable : true, align: 'center'},
		{display: 'Is Suspended', name : 'suspended', width : 120, sortable : true, align: 'center'}
		],
	searchitems : [
		{display: 'Employee ID', name : 'id', isdefault: true},
		{display: 'Full Name', name : 'full_name' },
		{display: 'Email', name : 'email' },
		{display: 'Privilege', name : 'privilege' },
		{display: 'Is Online', name : 'online' },
		{display: 'Is Suspended', name : 'suspended' },
		],
	buttons:[   
		 {name:'ADD',bclass:'add',onpress:add_account},		 		 
         {separator:true},
		 {name:'DELETE',bclass:'delete_account',onpress:delete_account},		 		 
         {separator:true}   		 
		],   
	singleSelect: true,
	sortname: "id",
	sortorder: "asc",
	usepager: true,
	title: 'Employee Accounts',
	useRp: true,
	rp: 30,
	showTableToggllinueBtn: true,
	width: 'auto',
	height: 300,
	resizable:false,
	blockOpacity:0.5, 
	onDoubleClick:edit_account
	});
	
}

function rowDbclick(row, g, p){
	
	if(oActiveWindows != null){
		displayMessage(oActiveWindows,"Please close current active window and try again.");
		return ;
	}
	displayProcessing(null);
	
	// Retrieve asset id
	var asset_id = $($(row).children("td")[0]).children("div").html();
	
	oSelectedRow = row; //Assign the selected row obj to the global variable
	
	// Initial asset editor window
	initial_asset_editor(asset_id);

}
function edit_account(row, g, p){
	initialAccountEditor(false,row);
}
function add_account(){
	initialAccountEditor(true,null);
	
}
function delete_account(com, grid){
	if(oGrids.account_table.selectedRows().length == 0){
		displayMessage(oSettingWindows.oChildWindo,"No account is selected. Please try again.");
		return 0;
	}
	confirmToDelete(oSettingWindows.oChildWindow,"Accounts",(oGrids.account_table.selectedRows()[0][0]).Value);
	$("#account_table").flexOptions({qtype:"",query:""}).flexReload();
}
function reset_filters(){
	$("#asset_table").flexOptions({qtype:"",query:""}).flexReload();
}
function initialAccountEditor(newAccount,row){
	var child = oWM.createWindow({
            title: "Edit Account",
            bodyContent: $('#account_template').html(),
            footerContent:'<button id="btn-submit" type="button" class="btn btn-primary" data-dismiss="window">Submit</button>'+
						  '<button id="btn-close" type="button" class="btn btn-default" data-dismiss="window">Close</button>'
        });

    oSettingWindows.oChildWindow.setBlocker(child);
	child.getElement().find('#privilege-group label').on("change",function(event){
		event.preventDefault();
		var options = child.getElement().find('#privilege-group').find('label');
		for(var i = 0; i < options.length; i++){
			$(options[i]).removeClass('btn-primary');
			$(options[i]).addClass('btn-default');
		}
		$(this).addClass('btn-primary');
		$(this).removeClass('btn-default');
		
	});
	child.getElement().find('#suspended-group label').on("change",function(event){
		event.preventDefault();
		var options = child.getElement().find('#suspended-group').find('label');
		for(var i = 0; i < options.length; i++){

			$(options[i]).removeClass('btn-primary');
			$(options[i]).addClass('btn-default');
		}
		$(this).addClass('btn-primary');
		$(this).removeClass('btn-default');
		
	});
	child.getElement().find('#btn-submit').on("click",function(){
		console.log("submit");
		var oJsonObj = [];
		oJsonObj[0] = child.getElement().find('#e-id').val().toUpperCase();
		oJsonObj[1] = child.getElement().find('#e-fullname').val();
		oJsonObj[2] = child.getElement().find('#e-email').val();
		oJsonObj[3] = $(child.getElement().find('#privilege-group').find('.active').find('input')).val();
		oJsonObj[4] = ($(child.getElement().find('#suspended-group').find('.active').find('input')).val() == 'yes'?"1":"0");
		
		displayProcessing(oSettingWindows.oChildWindow);
		if(newAccount == false){
			ajaxCall("updateAccount",JSON.stringify(oJsonObj));
			$("#account_table").flexOptions({qtype:"",query:""}).flexReload();
		}else{
			
			if(!(oJsonObj[0] in oDataSource.aAccountList)){
				ajaxCall("insertAccount",JSON.stringify(oJsonObj));
				$("#account_table").flexOptions({qtype:"",query:""}).flexReload();
			}else{
				alert("The id is already exist.");
			}
		}
		reloadDataSource(4);
	});
	if(newAccount == false){
		var tds = $(row).find('td');
		child.getElement().find("#e-id").val($(tds[0]).text());
		child.getElement().find("#e-fullname").val($(tds[1]).text());
		child.getElement().find("#e-email").val($(tds[2]).text());

		
		$("#option1").removeClass('active');
		$("#option1").removeClass('btn-primary');
		$("#option1").addClass('btn-default');
		$("#option"+$(tds[3]).text()).addClass('active');
		$("#option"+$(tds[3]).text()).addClass('btn-primary');
		if($(tds[5]).text()=='Yes'){
			$("#option_no").removeClass('active');
			$("#option_no").removeClass('btn-primary');
			$("#option_no").addClass('btn-default');
			
			$("#option_yes").addClass('btn-primary');
			$("#option_yes").addClass('active');
		}
	}
	
}

function new_asset(com,grid){
	if(privilege == 1){
		displayMessage(null,"You don't have permission to add the asset.");
		return;
	}
	if(oActiveWindows != null){
		displayMessage(oActiveWindows,"Another pop up window is open. Please close the window and try again.");
		return;
	}
	initial_asset_editor(null);
	
	
}
function custom_export(com, grid){
	if(oActiveWindows != null){
		displayMessage(oActiveWindows,"Another pop up window is open. Please close the window and try again.");
		return;
	}
	console.log((oGrids.asset_table[0]).p);
	oActiveWindows = oWM.createWindow({
			title: "Custom Export",
		bodyContent: $("#export_template").html(),
		footerContent: '<button type="button" class="btn btn-default" id="btn-cancel" data-dismiss="window">'+
					   '<span class="glyphicon glyphicon-home"></span>Close</button>'
					  
	});
	$(oActiveWindows.$el[0]).css({"top":"17%"});
	oActiveWindows.getElement().find('#btn-cancel').on("click",function(){oActiveWindows = null;});
	oActiveWindows.getElement().find('#select-all').on("click",function(){
		var checkboxs = oActiveWindows.getElement().find("#export-fields-container > table input");
		if($(this).is(":checked")){
			$(checkboxs).each(function(){
				if(!$(this).is(":checked")){
					$(this).prop("checked",true);
				}
			});
		}else{
			$(checkboxs).each(function(){
				if($(this).is(":checked")){
					$(this).prop("checked",false);
				}
			});
		}
	});
	oActiveWindows.getElement().find('#btn-export').on("click",function(){
		var fields_list = {};
		var index = 0;
		oActiveWindows.getElement().find("#export-fields-container > table input").each(function(){
			if($(this).is(":checked")){
				fields_list[index++] = $(this).parent().text();
			}
		});
		var where = ((oGrids.asset_table[0]).p.query != "")? "&where=" +  (oGrids.asset_table[0]).p.query : "";
		displayProcessing(oActiveWindows);
		window.location.href="http://localhost/operation/php/export_excel.php?export_fields="+JSON.stringify(fields_list)+where;
		
	});
}

function custom_filter(){
	
	if(oActiveWindows != null){
		displayMessage(oActiveWindows,"Another pop up window is open. Please close the window and try again.");
		return;
	}
	oActiveWindows = oWM.createWindow({
			title: "Filters",
		bodyContent: $("#filter_template").html(),
		footerContent: '<button type="button" class="btn btn-default" id="btn-cancel" data-dismiss="window">'+
					   '<span class="glyphicon glyphicon-home"></span>Close</button>'
					  
	});
	$(oActiveWindows.$el[0]).css({"top":"17%"});
	oActiveWindows.getElement().find('#btn-cancel').on("click",function(){oActiveWindows = null;});
	oActiveWindows.getElement().find('.filter-buttons').on("click",function(){
		console.log($(this).attr("data-filter-type"));
		var filter_type = $(this).attr("data-filter-type");
		var filter_value = ($(this).text().split(" ")[0]).trim();
		$("#asset_table").flexOptions({qtype:"filter",query:(filter_type +"="+filter_value)}).flexReload();
		oActiveWindows.close();
		oActiveWindows = null;
	});
	
	
}
function custom_search(){
	if(oActiveWindows != null){
		displayMessage(oActiveWindows,"Another pop up window is open. Please close the window and try again.");
		return;
	}
	oActiveWindows = oWM.createWindow({
			title: "Custom Filters",
		bodyContent: "<button type='button' id='btn-add-condition' class='btn btn-primary'>Add More</button><button type='button' id='btn-apply-filter' class='btn btn-primary' >Apply</button><br /><div id='filter-list'>"+searchCondition+"</div>",
		footerContent: '<button type="button" class="btn btn-default" id="btn-cancel" data-dismiss="window">'+
					   '<span class="glyphicon glyphicon-home"></span>Close</button>'
					  
	});
	$(oActiveWindows.$el[0]).css({"top":"17%"});
	oActiveWindows.getElement().find('#btn-cancel').on("click",function(){oActiveWindows = null;});
	oActiveWindows.getElement().find('.btn-delete-filter').on("click",function(){$(this).parent().remove();	});
	oActiveWindows.getElement().find('#btn-add-condition').on("click",function(){
		oActiveWindows.getElement().find('#filter-list').append(searchCondition);
		oActiveWindows.getElement().find('.btn-delete-filter').on("click",function(){$(this).parent().remove();	});
		oActiveWindows.getElement().find('.filter-keyword').on("click",function(event){event.preventDefault(); $(this).popover({trigger: 'focus' }) });
	});
	
	oActiveWindows.getElement().find('.filter-keyword').on("click",function(event){event.preventDefault(); $(this).popover({trigger: 'focus' }) });
	oActiveWindows.getElement().find('#btn-apply-filter').on("click",function(){
		var conditions = oActiveWindows.getElement().find('.custom-filter-condition');
		var filter_list = {};
		
		$(conditions).each(function(){
			var filter_field = $(this).children('select').find(":selected");
			var filter_value = $(this).children('input').val();
			if(filter_value != ""){
				if($(filter_field).attr("data-field-typ") != 'int'){
					filter_value = "'" + filter_value + "'";
				}
				filter_list[$(filter_field).val()] = filter_value;
			}
			
		});
		if(filter_list.length != 0){
			$("#asset_table").flexOptions({dataType:"json",qtype:"custom_filter",query:JSON.stringify(filter_list)}).flexReload();
		}
	});
	oActiveWindows.getElement().find('select').on("click",function(event){
		event.preventDefault();
		console.log($(this).find(":selected").attr("data-field-typ"));
		if($(this).find(":selected").attr("data-field-typ") == "int"){
			$(this).parent().find("input").attr("data-content","Number Only");
		}else if($(this).find(":selected").attr("data-field-typ") == "date"){
			$(this).parent().find("input").attr("data-content","Date format : YYYY-MM-DD");
		}else{
			$(this).parent().find("input").attr("data-content","Any type of characters except ' or \" ");
		}
	});
}

// Initial asset editor window
function initial_asset_editor(asset_id){
	var calibration_id = null;
	oActiveWindows = oWM.createWindow({
			title: "Asset Editor",
		bodyContent: $("#asset_editor_template").html(),
		footerContent: '<button type="button" class="btn btn-success" id="btn-validate">'+
					   '<span class="glyphicon glyphicon-eye-open"></span>Validate</button>'+
					   '<button type="button" class="btn btn-default" id="btn-cancel" data-dismiss="window">'+
					   '<span class="glyphicon glyphicon-home"></span>Close</button>'+
					   '<button type="button" id ="btn_submit" class="btn btn-primary" disabled>'+
					   '<span class="glyphicon glyphicon-send"></span>Submit</button>'
	});
	
	
	// Initialize the current assigned user list
	var $user = oActiveWindows.getElement().find('#id_13');
	$user.find('[value="X"]').remove(); // remove all options;
	$user.append("<option>--User--</option>");
	
	// Initial the owner list
	var $owner = oActiveWindows.getElement().find('#id_15');
	$owner.find('[value="X"]').remove(); // remove all options
	$owner.append("<option>--Owner--</option>");
	
	//Populate data to drop list
	for(var key in oDataSource.aAccountList){
		var val = oDataSource.aAccountList[key];
		$owner.append("<option data-id="+key+">"+val+"</option>");
		$user.append("<option data-id="+key+">"+val+"</option>");
	}
	
	// Initialize the team drop-down list
	var $team = oActiveWindows.getElement().find('#id_14');
	$team.find('[value="X"]').remove(); // remove all options;
	$team.append("<option>--Team belong to --</option>");
	
	//Populate data to drop list
	for(var key in oDataSource.aTeamList){
		var val = oDataSource.aTeamList[key];
		$team.append("<option data-id="+key+">"+val+"</option>");
	}
	
	// Initialize the location drop-down list
	var $location = oActiveWindows.getElement().find('#id_16');
	$location.find('[value="X"]').remove(); // remove all options;
	$location.append("<option>--Location--</option>");
	
	for(var key in oDataSource.aLocationList){
		var val = oDataSource.aLocationList[key];
		$location.append("<option data-id="+key+">"+val+"</option>");
	}
	
	// Initialize the status drop-down list
	var $status = oActiveWindows.getElement().find('#id_4');
	$status.find('[value="X"]').remove(); // remove all options;
	
	for(var key in oDataSource.aStatus){
		var val = oDataSource.aStatus[key];
		$status.append("<option data-id="+key+">"+val+"</option>");
	}
	
	
	// Initial popover feature 
	oActiveWindows.getElement().find('input, textarea').each(function(){$(this).popover({trigger: 'focus' })});
	
	// Initial datatime picker feature
	oActiveWindows.getElement().find('.date_fileds').datetimepicker({format: 'yyyy-mm-dd'});

	$("#received_date, #hw-start-date,#hw-end-date,#sw-start-date,#sw-end-date,#last-cal-date,#next-cal-date,#mfr-contract-end-date").datetimepicker({pickTime: false});

	if(privilege == 1){
		var oButtons = oActiveWindows.getElement().find(".window-footer").find("button");
		$(oButtons[0]).attr("disabled","disabled");	//validation button
		$(oButtons[2]).attr("disabled","disabled");	//submit button
	}
	
	
	
	// Define cancel button event
	oActiveWindows.getElement().find('#btn-cancel').on("click",function(){ 	oActiveWindows = null;	});
	
	// Define validation button event
	oActiveWindows.getElement().find('#btn-validate').on("click",function(){
		$.getScript('js/validation.js', function() {
			cleanupMarks(oActiveWindows);
			var iBasic_error_count = validate(oActiveWindows,basic_fields);
			var lLicense_error_count =  0;
			var lCalibration_error_count =  0;

		   if(oActiveWindows.getElement().find('#id_18').is(':checked')){
				lLicense_error_count =  validate(oActiveWindows,license_fields);
				var hw_start = oActiveWindows.getElement().find('#id_20');
				var hw_end = oActiveWindows.getElement().find('#id_21');
				var sw_start = oActiveWindows.getElement().find('#id_23');
				var sw_end = oActiveWindows.getElement().find('#id_24');
				
				
				if((hw_start.val() != "" || hw_start.val() != "undefined") && (hw_end.val() != "" || hw_end.val() != "undefined")){
					if(hw_start.val() > hw_end.val()){
						$(hw_start).parent().parent().addClass("has-error has-feedback");
						$(hw_start).parent().parent().css("clear","both").append('<small class="help-block col-lg-offset-3 " style="">The start date cannot be greater than end date.</small>');
						
						$(hw_end).parent().parent().addClass("has-error has-feedback");
						$(hw_end).parent().parent().css("clear","both").append('<small class="help-block col-lg-offset-3 " style="">The start date cannot be greater than end date.</small>');
						
						lLicense_error_count += 1;
					}
				}
				if((sw_start.val() != "" || sw_start.val() != "undefined") && (sw_end.val() != "" || sw_end.val() != "undefined")){
					if(sw_start.val() > sw_end.val()){
						$(sw_start).parent().parent().addClass("has-error has-feedback");
						$(sw_start).parent().parent().css("clear","both").append('<small class="help-block col-lg-offset-3 " style="">The start date cannot be greater than end date.</small>');
						
						$(sw_end).parent().parent().addClass("has-error has-feedback");
						$(sw_end).parent().parent().css("clear","both").append('<small class="help-block col-lg-offset-3 " style="">The start date cannot be greater than end date.</small>');
						
						lLicense_error_count += 1;
					}
				}
		   }
		   if(oActiveWindows.getElement().find('#id_19').is(':checked')){
				lCalibration_error_count =  validate(oActiveWindows,calibration_fields);
				var last_calibration = oActiveWindows.getElement().find('#id_32');
				var next_calibration_mfg = oActiveWindows.getElement().find('#id_33');
				var next_calibration_wch = oActiveWindows.getElement().find('#id_34');
				
				if( last_calibration.val() != "" || last_calibration.val() != "undefined"){
					if(next_calibration_mfg.val() != "" || next_calibration_mfg.val() != "undefined"){
						if(last_calibration.val() > next_calibration_mfg.val()){
							$(last_calibration).parent().parent().addClass("has-error has-feedback");
							$(last_calibration).parent().parent().css("clear","both").append('<small class="help-block col-lg-offset-3 " style="">The last calibration date cannot be greater than end date.</small>');
						
							$(next_calibration_mfg).parent().parent().addClass("has-error has-feedback");
							$(next_calibration_mfg).parent().parent().css("clear","both").append('<small class="help-block col-lg-offset-3 " style="">The next calibration date cannot be greater than end date.</small>');
							lCalibration_error_count += 1;
						}
					}
					if(next_calibration_wch.val() != "" || next_calibration_wch.val() != "undefined"){
						if(last_calibration.val() > next_calibration_wch.val()){
							if(!(last_calibration).parent().parent().hasClass("has-error")){
								$(last_calibration).parent().parent().addClass("has-error has-feedback");
								$(last_calibration).parent().parent().css("clear","both").append('<small class="help-block col-lg-offset-3 " style="">The last calibration date cannot be greater than end date.</small>');
							}
						
							$(next_calibration_wch).parent().parent().addClass("has-error has-feedback");
							$(next_calibration_wch).parent().parent().css("clear","both").append('<small class="help-block col-lg-offset-3 " style="">The next calibration date cannot be greater than end date.</small>');
							lCalibration_error_count += 1;
						}
					}
				}
				if(oActiveWindows.getElement().find('#id_29').is(':checked')){
					var refInternal = oActiveWindows.getElement().find('#id_31');
					if( $(refInternal).val() == "undefined" || $(refInternal).val() == ""){
						$(refInternal).parent().addClass("has-error has-feedback");
						$(refInternal).parent().append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
						$(refInternal).parent().css("clear","both").append('<small class="help-block col-lg-offset-3 " style="">When Calibration Reference is checked, this field cannot be required.</small>');
					}
				}
				
		   }

		   var aLIs = oActiveWindows.getElement().find('.asset_editor_tab_control >ul> li');
		   $(aLIs).each(function(){				
				$(this).on("click",function(){
					if(!$(this).hasClass("active")){
						var span = $(this).find("a > span");
						if($(span).hasClass("hide") == false){
							$(span).addClass("hide");
						}
					}
				});
		   });
		  
		   if(iBasic_error_count != 0){
				if($(aLIs[0]).hasClass("active") == false){
					var span = $(aLIs[0]).find("a > span");
					$(span).removeClass("hide");
					$(span).html(iBasic_error_count);
				}
		   }
		   if(lLicense_error_count != 0){
				if($(aLIs[2]).hasClass("active") == false){
					var span = $(aLIs[2]).find("a > span");
					$(span).removeClass("hide");
					$(span).html(lLicense_error_count);
				}
		   }
		   if(lCalibration_error_count != 0){
		   
				console.log(aLIs[3]);
				if($(aLIs[3]).hasClass("active") == false){
					var span = $(aLIs[3]).find("a > span");
					$(span).removeClass("hide");
					$(span).html(lCalibration_error_count);
				}
		   }
		   if(iBasic_error_count == 0 && 
		      lLicense_error_count == 0 && 
			  lCalibration_error_count == 0){
				oActiveWindows.getElement().find('#btn_submit').removeAttr('disabled');
		   }else{
				if(!oActiveWindows.getElement().find('#btn_submit').is(":disabled")){
					oActiveWindows.getElement().find('#btn_submit').attr("disabled",true);
				}
		   }
		});
		
	});	// end of validation button definition
	
	// Define submit button event
	oActiveWindows.getElement().find('#btn_submit').on("click",function(){
		displayProcessing(oActiveWindows);
		var oJsonOjb = {};
		oJsonOjb["id_0"] = parseInt(asset_id);
		var field_id = "";
		var field_value = "";
		var field_type = "";
		oActiveWindows.getElement().find("input, textarea, select").each(function(i){
			field_id = $(this).attr("id");
			if(field_id == "id_4" || field_id == "id_13" || field_id == "id_14" || field_id == "id_15" || field_id == "id_16"){
				if(field_id == "id_4"){				
					field_value = "'" + $(this).val() + "'";	// select tag option value
				}else if(field_id == "id_14" || field_id == "id_16"){			
					field_value = parseInt($(this).find(":selected").attr("data-id"));	// custom data value from attribute and the value is integer type
				}else{		
					field_value = "'"+$(this).find(":selected").attr("data-id")+"'";	// custom data value from attribute and the value is string type
				}
			}else if(field_id == "id_18" ||field_id == "id_19" ||field_id == "id_28" || field_id == "id_29"){
				field_value = $(this).is(":checked") ? 1 : 0;
			}else{
				field_type = $(this).attr("data-type");
				var s = $(this).val();
				field_value = (s == '' || s == 'undefined')? null : s;
				
				if(field_type == "string" || field_type == "date"){
					field_value = (field_value == null)? "NULL" : "'" + field_value + "'";
				}else{
					field_value = (field_value == null)? "NULL" :  parseInt(field_value);
				}
			}
			oJsonOjb[field_id] = field_value;
			
		});
		
		if( typeof asset_id != 'undefined' && asset_id != null){
			ajaxCall("update_asset",JSON.stringify(oJsonOjb));
			displayProcessing(oActiveWindows);
		}else{
			var result = ajaxCall("insert_asset",JSON.stringify(oJsonOjb));
			displayProcessing(oActiveWindows);
			if(result != null){
				if(result.asset_id != null){
					asset_id = result.asset_id;
					oActiveWindows.getElement().find('#btn_asset_attachments').removeAttr("disabled");
					oActiveWindows.getElement().find('#btn_calibration_attachment').removeAttr("disabled");
				}
				
			}
			
		}
		
		
		$(this).attr("disabled","disabled");
		$("#asset_table").flexOptions().flexReload();
		
	}); // end of the submit button definition

	oActiveWindows.getElement().find('#btn_asset_attachments').on('click', function (event) {
		event.preventDefault();
        var child = oWM.createWindow({
            title: "Attachments",
            bodyContent: $("#attachments_template").html(),
            footerContent: '<button type="button" class="btn btn-default" data-dismiss="window">Close</button>'
        });
        oActiveWindows.setBlocker(child);
		
		var aData = ajaxCall("getAssetDocList",JSON.stringify({'asset_id':asset_id}));
		
		for(var i = 0; i < aData.length; i++){
			var temp = aData[i];
			console.log(temp);
			createFileLink(asset_id,child.getElement(),temp,"asset");
		}
		child.getElement().find('#uploaded_file:file').on("change",function(){
			var file = $(this)[0].files[0];
			console.log(file);
			if(file.size > 10485760){
				alert("File size is too big");
				return;
			}
			
			var type = file.type;
			if(!(type.trim() == "application/pdf") && !(type.trim() == "application/msword")){
				alert("Invalid file type");
				return;
			}
			if(privilege !=1){
				child.getElement().find('#btn-start-upload').removeAttr('disabled');
			}
			
		});
		child.getElement().find('#btn-start-upload').on('click', function (event) {
			event.preventDefault();
			console.log(child.getElement().find('#uploaded_file')[0].files[0]);
			uploadFile(child.getElement(),"asset",asset_id);
			
		});
    });
	oActiveWindows.getElement().find('#btn_calibration_attachment').on('click', function (event) {
		event.preventDefault();
        var child = oWM.createWindow({
            title: "Attachments",
            bodyContent: $("#attachments_template").html(),
            footerContent: '<button type="button" class="btn btn-default" data-dismiss="window">Close</button>'
			
			
        });
        oActiveWindows.setBlocker(child);
		
		var aData = ajaxCall("getCalibrationDocList",JSON.stringify({'asset_id':asset_id}));
		
		for(var i = 0; i < aData.length; i++){
			var temp = aData[i];
			console.log(temp);
			createFileLink(asset_id,child.getElement(),temp,"calibration");
		}
		child.getElement().find('#uploaded_file:file').on("change",function(){
			var file = $(this)[0].files[0];
			console.log(file);
			if(file.size > 10485760){
				alert("File size is too big");
				return;
			}
			
			var type = file.type;
			if(!(type.trim() == "application/pdf") && !(type.trim() == "application/msword")){
				alert("Invalid file type");
				return;
			}
			if(privilege !=1){
				child.getElement().find('#btn-start-upload').removeAttr('disabled');
			}
			
		});
		child.getElement().find('#btn-start-upload').on('click', function (event) {
			event.preventDefault();
			console.log(child.getElement().find('#uploaded_file')[0].files[0]);
			uploadFile(child.getElement(),"calibration",asset_id);
			
		});
		
    });
	
	// id is either undefined or null it means the creating without populate data to the
	// editor form 
	if( typeof asset_id != 'undefined' && asset_id != null){
		// instead of existing row data to form, I make ajax call to query to the database 
		// which makes the asset editor form has up to date data.
		var aData = ajaxCall("getAssetInfo",JSON.stringify({'asset_id':asset_id}));
		
		
		populateDataToForm(aData);
		
		oActiveWindows.getElement().find("#btn_asset_attachments").removeAttr("disabled");
		oActiveWindows.getElement().find("#btn_calibration_attachment").removeAttr("disabled");
		
		aData = ajaxCall("getAssetLogs",JSON.stringify({'asset_id':asset_id}));
	
		for(i = 0; i < aData.length; i++){
			oActiveWindows.getElement().find('#history').append(createEventDiv(aData[i]));
		}
	}
	// handle the license check box
	// enable to the corresponding tab control when license is checked.
	var maintenance = oActiveWindows.getElement().find('#id_18');
	var tab_3 = oActiveWindows.getElement().find('.nav-tabs li:nth-child(3)');
	if(!maintenance.is(':checked')){
		$(tab_3).addClass("disabled");
		$(tab_3).children('a').removeAttr('data-toggle');

	}
	$(maintenance).on("click",function(){
		if(!$(this).is(':checked')){
			$(tab_3).addClass("disabled");
			$(tab_3).children('a').removeAttr('data-toggle');
		}else{
			$(tab_3).removeClass("disabled");
			$(tab_3).children('a').attr("data-toggle","tab")
		}
	});
	
	// handle the calibration check box
	// enable to the corresponding tab control when calibration is checked.
	var calibration = oActiveWindows.getElement().find('#id_19');
	var tab_4 = oActiveWindows.getElement().find('.nav-tabs li:nth-child(4)');
	if(!calibration.is(':checked')){
		$(tab_4).addClass("disabled");
		$(tab_4).children('a').removeAttr('data-toggle');

	}
	$(calibration).on("click",function(){
		if(!$(this).is(':checked')){
			$(tab_4).addClass("disabled");
			$(tab_4).children('a').removeAttr('data-toggle');
			
		}else{
			$(tab_4).removeClass("disabled");
			$(tab_4).children('a').attr("data-toggle","tab");
		}
	});
	// handle Calibration Reference check box
	// auto fill out the date when calibration reference check box is checked
	var calReference = oActiveWindows.getElement().find('#id_29');
	oActiveWindows.getElement().find('#calibration_info #id_29').on("click",function(){
	console.log("Notification");
		if($(this).is(':checked')){
			var d = new Date();
			console.log("Test");
			oActiveWindows.getElement().find('#id_30').val(d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate());
		}else{
			oActiveWindows.getElement().find('#id_30').val("");
		}
	});
}

/**
 *	Create attachment file link.
 *	
 */
function createFileLink(owner_id,target,file_name,directory){
	var text = "<tr><td><a target='_blank' href='attachments/"+directory+"/"+file_name+"'>"+file_name+"</a></td>"+
						   "<td><button type='button' class='btn btn-danger btn-delete-row' ><span class='glyphicon glyphicon-trash'></span>Delete</button></td></tr>";
	target.find('#attachment-list tbody').append(text);
	
	target.find('.btn-delete-row').on("click",function(){
		var fileName = $(this).parent().parent().find("td").find("a").text();
		if(ajaxCall("deleteAttachment",JSON.stringify({'asset_id':owner_id,'file_name':fileName,"file_category":directory}))){
			$(this).parent().parent().remove();
		}
	});
}
function createEventDiv(oEvent){
	var eDiv = "<div class='event-log'>"+
			   "<div>Modified Date : " + oEvent.event_time + "</div>" +
			   "<div><span>Modified By : " + oEvent.modified_by + "</div>" +
			   "<div>Email : <a href='mailto:" + oEvent.email + "'>"+oEvent.email+"</a></div>" +
			   "<div>Event Actions : " + oEvent.event + "</div>" +
			   "</div>";
	return eDiv;
}
function uploadFile(aElements,directory,owner_id){
	console.log(directory);
	var file = $(aElements).find('#uploaded_file')[0].files[0];

	
	$("<div class='progress'><div class='progress-bar progress-bar-success' role='progressbar' aria-valuemin='0' aria-valuemax='100' style='width:0%;'></div></div>").insertAfter("#btn-start-upload");
	var formData = new FormData();
	formData.append( "uploaded_file", file);
	formData.append("directory",directory);
		
	var xhr = new XMLHttpRequest();
	
	//Upload Progress
	xhr.upload.addEventListener("progress", function(oEvent){
		if (oEvent.lengthComputable) {
			var percentComplete = (oEvent.loaded / oEvent.total)* 100;
			$(aElements.find('.progress .progress-bar')).css({ "width": percentComplete + "%" });
		}
	}, false);
	xhr.onreadystatechange = function() {  
		 if (xhr.readyState === 4) {  
			if (xhr.status === 200) {  
				if(xhr.responseText === 'true'){	
					if(directory === 'asset'){
						oAttachmentList.aAssetFileList.push(file.name);
						createFileLink(owner_id,aElements,file.name,directory);
						ajaxCall("insertAssetDocument",JSON.stringify({'asset_id':owner_id, 'file_name':file.name}));
					}else if(directory === 'calibration'){
						oAttachmentList.aCalibrationFileList.push(file.name);
						createFileLink(owner_id,aElements,file.name,directory);
						ajaxCall("insertCalibrationDocument",JSON.stringify({'asset_id':owner_id, 'file_name':file.name}));
					}
					aElements.find('#uploaded_file:file').val("");
					$(aElements.find(".progress")).remove();
					$(aElements).find('#btn-start-upload').attr("disabled","disabled");
				}else{
					console.log(xhr.responseText);  
				}
			} else {  
				console.log("Error", xhr.statusText);  
			}  
		}  
	};  
	xhr.open("POST", 'php/file_upload.php', true);
	xhr.send(formData);
}
//Populate the data to the asset editor form
function populateDataToForm(aData ){
	console.log(aData.length);
	var temp;
	for(i = 1; i < aData.length; i++){
		temp = aData[i];
		if(i == 13 || i == 14 || i == 15 || i == 16){
			if(temp == null || temp == ""){
				$("#id_" + i + " [name=options]").val(0);
				continue;
			}
		}
		if(i == 13 || i == 15){
			$("#id_"+i).val(oDataSource.aAccountList[aData[i]]);
			continue;
		}
		if(i == 14){
			$("#id_"+i).val((aData[i] !=0)?oDataSource.aTeamList[aData[i]]:0);
			continue;
		}
		if(i == 16){
			$("#id_"+i).val(oDataSource.aLocationList[aData[i]]);
			continue;
		}
		if(i == 18 || i == 19 || i == 28 || i == 29){
			if(aData[i] == 1 || (aData[i]) == 1){
				$("#id_"+i).prop('checked', true);
			}
		}
		$("#id_"+i).val(aData[i]);
			
	}
	
}
function loadDataSource_AccountList(){
	var aResult = ajaxCall('accounts',null);
	if(aResult == null){
		console.log("Result is null");
		return;
	}
	$.each(aResult, function(key, val) {
		oDataSource.aAccountList[key] = val;
	});
}

function loadDataSource_LocationList(){
	var aResult = ajaxCall('locations',null);
	if(aResult == null){
		console.log("Result is null");
		return;
	}
	$.each(aResult, function(key, val) {
		oDataSource.aLocationList[key] = val;
	});
}
function loadDataSource_status(){
	var aResult = ajaxCall('status',null);
	if(aResult == null){
		console.log("Result is null");
		return;
	}
	$.each(aResult, function(key, val) {	
		oDataSource.aStatus[key] = val;
		
	});
}
function loadDataSource_teamList(){
	var aResult = ajaxCall('teamList',null);
	if(aResult == null){
		console.log("Result is null");
		return;
	}
	$.each(aResult, function(key, val) {
		oDataSource.aTeamList[key] = val;
		
	});
}
function confirmToDelete(parentWindow,type, id){
	var child = oWM.createWindow({
        title: "Confirm To Delete",
        bodyContent: "<p><h4 style='color:red;'>WARNNING ! </h4><br />The field associate with ["+((type == undefined)?"":type)+"] will automatically set to NULL.<br /><br />"+
					 "Continue : continue to delete. <br /> Cancel : cancel and close the window.",
        footerContent:"<button id='btn-continue-delete' type='button' class='btn btn-primary' data-dismiss='window'>Continue</button><button id='btn-close' type='button' class='btn btn-default' data-dismiss='window'>Close</button>"
    });
	child.getElement().find('.window-header').remove();
	$(child.$el[0]).css({"top":"25%"});
	parentWindow.setBlocker(child);
	
	child.getElement("#btn-continue-delete").on("click",function(){
		
		var oJsonObj = {};
		oJsonObj[0] = type;
		oJsonObj[1] = id;
		
		displayProcessing(parentWindow);
		ajaxCall("deleteDatsource",JSON.stringify(oJsonObj));
		if(type == "Locations"){
			reloadDataSource(1);
		}else if(type == "Status"){
			reloadDataSource(2);
		}else if(type == "Team"){
			reloadDataSource(3);
		}
	});
}
function displayMessage(parentWindow,message){
	if(oMessageWindow != null){
		return;
	}
	oMessageWindow = oWM.createWindow({
        title: "Message",
        bodyContent: "<p><h4 style='color:red;'>WARNNING ! </h4><br />" + message +"</p>",
        footerContent:"<button id='btn-close' type='button' class='btn btn-default' data-dismiss='window'>Close</button>"
    });
	oMessageWindow.getElement().find("#btn-close").on("click",function(){oMessageWindow = null;});;
	oMessageWindow.getElement().find('.window-header').remove();
	$(oMessageWindow.$el[0]).css({"top":"25%"});
	if(parentWindow != null){
		parentWindow.setBlocker(oMessageWindow);
	}
}
function displayProcessing(parentWindow){
	var child = oWM.createWindow({
        title: "Message",
        bodyContent: '<div class="progress progress-popup"><div class="progress-bar progress-bar-striped"></div></div>',
        footerContent:"<button id='btn-close' type='button' class='btn btn-default' data-dismiss='window'>Close</button>"
    });
	child.getElement().find('.window-header').remove();
	child.getElement().find('.window-footer').remove();
	child.getElement().find('.window-body').css("width","400px");
	child.getElement().find('.progress-bar').css("width","100%");
	if(parentWindow != null){
		parentWindow.setBlocker(child);
	}
	setTimeout(function() {
    child.close();
  }, 500);
}
function reloadDataSource(type){
	console.log(type);
	switch(type){
		case 1:	// location
			oDataSource.aLocationList.length = 0;
			loadDataSource_LocationList();
			$(oSettingWindows.oChildWindow.getElement().find('#setting-location')).empty();
			var options = "";
			for(var key in oDataSource.aLocationList){
				options += ("<option data-id='"+key+"'>" + oDataSource.aLocationList[key] + "</option>");
			}
			$(oSettingWindows.oChildWindow.getElement().find('#setting-location')).append(options);
			$(oSettingWindows.oChildWindow.getElement().find('#setting-location')).trigger("chosen:updated");
			break;
		case 2:
			oDataSource.aStatus.length = 0;
			loadDataSource_status();
			$(oSettingWindows.oChildWindow.getElement().find('#setting-status')).empty();
			
			var options = "";
			for(var key in oDataSource.aStatus){
				options += ("<option data-id='"+key+"'>" + oDataSource.aStatus[key] + "</option>");
			}
			$(oSettingWindows.oChildWindow.getElement().find('#setting-status')).append(options);
			$(oSettingWindows.oChildWindow.getElement().find('#setting-status')).trigger("chosen:updated");
			break;
		case 3:
			oDataSource.aTeamList.length = 0;
			loadDataSource_teamList();
			$(oSettingWindows.oChildWindow.getElement().find('#setting-team')).empty();
					
			var options = "";
			for(var key in oDataSource.aTeamList){
				options += ("<option data-id='"+key+"'>" + oDataSource.aTeamList[key] + "</option>");
			}
			$(oSettingWindows.oChildWindow.getElement().find('#setting-team')).append(options);
			$(oSettingWindows.oChildWindow.getElement().find('#setting-team')).trigger("chosen:updated");
			break;
		case 4:
			oDataSource.aAccountList.length = 0;
			loadDataSource_AccountList();
			break;
	}
	
}
// ajax call base function 
function ajaxCall(query_type,aData){
	var aResult = null;
	$.ajax({
		async: false,
		dataType: "json",
		url: "php/DBHelper.php",
		timeout: 3000,
		data: {qtype:query_type,json_data:(aData != null)?aData:""},
		success: function(response) {
			aResult = response;
			if(query_type == "updateLocation" || query_type == "insertLocation"){
				reloadDataSource(1);
			}else if(query_type == "updateStatus" || query_type == "updateStatus"){
				reloadDataSource(2);
			}else if(query_type == "updateTeam" || query_type == "updateTeam"){
				reloadDataSource(3);
			}
			
		},
		error: function(jqXHR, exception) {
            if (jqXHR.status === 0) {
                alert('Not connect.\n Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found. [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (exception === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (exception === 'timeout') {
                alert('Time out error.');
            } else if (exception === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error.\n' + jqXHR.responseText);
            }
			console.log("Error");
			return null;
        }
	});
	return aResult;
}

var searchCondition = "<div class='custom-filter-condition'><select class='form-control'><optgroup label='Basic Info'>"+
"<option data-field-typ='int'>Asset ID</option><option data-field-typ='string'>Wistron Tag</option><option data-field-typ='string'>RIM Tag</option>"+
"<option data-field-typ='string'>Serial Number</option><option data-field-typ='string'>Status</option><option data-field-typ='string'>Model</option>"+
"<option data-field-typ='string'>Manufacturer</option><option data-field-typ='string'>Description</option><option data-field-typ='date'>Received Date</option>"+
"<option data-field-typ='string'>PR</option><option data-field-typ='string'>PO</option><option data-field-typ='int'>Expected Life</option>"+
"<option data-field-typ='string'>User</option><option data-field-typ='string'>Team</option><option data-field-typ='string'>Owner</option>"+
"<option data-field-typ='string'>Location</option><option data-field-typ='string'>Comments</option><option data-field-typ='int'>License Required</option>"+
"<option data-field-typ='int'>Calibration Required</option>"+
"</optgroup>"+
"<optgroup label='HW & SW License Info'>"+
"<option data-field-typ='date'>HW Start Date</option><option data-field-typ='date'>HW End Date</option><option data-field-typ='int'>HW Cost</option><option data-field-typ='date'>SW Start Date</option>"+
"<option data-field-typ='date'>SW End Date</option><option data-field-typ='int'>SW Cost</option><option data-field-typ='string'>Comments</option>"+
"</optgroup>"+
"<optgroup label='Calibration Info'>"+
"<option data-field-typ='string'>Calibration Number</option><option data-field-typ='int'>Notification Required</option><option data-field-typ='int'>Reference Checked</option>"+
"<option data-field-typ='date'>Checked Date</option><option data-field-typ='int'>Reference Interval</option><option data-field-typ='date'>Last Calibration Date</option>"+
"<option data-field-typ='date'>Next Calibration Date (mfg)</option><option data-field-typ='date'>Next Calibration Date (WCH)</option><option data-field-typ='int'>Calibration Interval</option>"+
"<option data-field-typ='int'>Calibration Cost</option><option data-field-typ='date'>Contract End Date</option><option data-field-typ='string'>Classification</option>"+
"<option data-field-typ='string'>Comments</option></optgroup></select><input type='text' class='form-control filter-keyword' data-placement='top' data-content='Number only'><button type='button' class='btn btn-danger btn-delete-filter' >Delete</button></div>";
