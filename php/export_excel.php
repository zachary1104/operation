<?php
	include 'DB.php';
	include 'field_table.php';
	
	$db = new DB;
	$log = new Log;
	if(isset($_GET['export_fields'])){
		$data = json_decode($_GET['export_fields'],true);
		$oWhere = isset($_GET['where'])?json_decode($_GET['where'],true) : null;
		$aRow = array();
		$output = "";
		$output .=implode(',', $data ) ."\r\n";
		$columns = "";
		$where = $oWhere != null? "where ":"";
		if($oWhere != null){
			
			$index =1;
			$count = count($oWhere);
			foreach ($oWhere as $key => $value){
			
				if($index != $count){
					if (strpos($value,"'") !== false) {
						$value = trim($value,"'");
						$where .= "LOWER(" .$field_table[$key]. ") like LOWER('%" . $value . "%') AND ";
					}else{
						$where .= $field_table[$key] . " = " . $value ." AND ";
						
					}
				}else{
					if (strpos($value,"'") !== false) {
						$value = trim($value,"'");
						$where .= "LOWER(" .$field_table[$key]. ") like LOWER('%" . $value . "%') ";
					}else{
						$where .= $field_table[$key] . " = " . $value ." ";
						
					}
				}
				$index++;
			}
		}
		foreach ($data as $key => $value){
			
			$columns .= $field_table[$value] . ",";
		}
		$columns = rtrim($columns, ",");
		$log->d("export : " . $columns);
		$count = count($data);
		if(($result = $db->execute_sql("SELECT $columns FROM full_asset_view $where")) != null ){
			while($row = $result->fetch_row()){
			$output .= '"' . implode('","', $row) . '"' . "\r\n";
			}
			
		}
		header("Content-type: application/ms-excel;charset=UTF-8"); 
		header("Cache-Control: no-store, no-cache"); 
		header('Content-Disposition: attachment; filename="asset_data.csv"');
		echo $output;
		exit;
	}
?>