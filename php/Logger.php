<?php
class Log{
	private $log_file;
	
	//constructor
	function Log(){
		try{
			// Set the default time zone
			date_default_timezone_set("America/Chicago"); 
			
			$log_file = date("m-d-Y").'.txt';
			$log_direcotry = getcwd()."/log";
			
			//Verify the log directory
			if(!file_exists($log_direcotry)){
				mkdir($log_direcotry,0777,true);
			}
			
			$this->log_file = $log_direcotry."/".$log_file;
			// verify the log file and create it if the file is not exist
			if(!file_exists($this->log_file)){
				$fp = fopen($this->log_file, 'x');
				fclose($fp);
			}
			
		}catch(Exception $e){
			error_log($e);
		}
	}

	private function write_to_file($log){
		try{
			$fp = fopen($this->log_file, 'a');
			fwrite($fp, $log.PHP_EOL);
			fclose($fp);
		}catch(Exception $e){
			error_log($e);
		}
	}
	/**
	 *	Logging the log with flag [DEBUG]
	 *	@param 
	 *			$event log message
	 */
	function d($event){
		$message = date("m-d-Y H:i:s").' [DEBUG] : '.$event;
		$this->write_to_file($message);
	}

	/**
	 *	Logging the log with flag [INFO]
	 *	@param 
	 *			$event log message
	 */
	function i($event){
		$message = date("m-d-Y H:i:s").' [INFO] : '.$event;
		$this->write_to_file($message);
	}

	/**
	 *	Logging the log with flag [ERROR]
	 *	@param 
	 *			$event log message

	 */
	function e($event){
		$bt = debug_backtrace();
		$caller = array_shift($bt);
		$file_name = $caller['file'];
		$line_no = $caller['line'];
		$message = date("m-d-Y H:i:s").' [ERROR] : '.$event."	\t\t\t\t".$file_name." on line : ".$line_no;
		$this->write_to_file($message);
	}
}

?>