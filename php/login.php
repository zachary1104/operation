<?php 
session_start();

include 'DB.php';
$log = new Log;
$db = new DB;
$log->i("Received the sign in authentication request. Retrieving sign in information....");



if(isset($_POST['wistron-id']) && isset($_POST['password'])){
	$log->i("Wistron-ID [".$_POST['wistron-id']."] found. ");
    $adServer = "ldap://wmx.wistron";
	
    $ldap = ldap_connect($adServer);
	
	if(!$ldap){
		$log->e("Unable to connect to LDAP server.");
		echo json_encode(false);
		return;
	}
	$log->i("Request the authentication from the Active Directory Server ....");
	/*
	if ($bind = @ldap_bind($ldap, "wmx\\".$_POST['wistron-id'], $_POST['password'])) {
		$log->i("Wistron ID [".$_POST['wistron-id']."] is verified. Retrieving permission information from the database server ...");
		$result = $db->getUserInfo("'".$_POST['wistron-id']."'");
		$_SESSION["login_id"] = $_POST['wistron-id'];
		$info = array();
		$info["full_name"] = $result[0];	// full name
		$info["email"] = $result[1];	// email
		$info["privilege"] = $result[2];	// privilege
		$info["login"] = true;
		if($result[3] == 1){
			$info["suspend"] = true;
			$_SESSION["userInfo"] = null;
		}else{
			$info["suspend"] = false;
			$_SESSION["userInfo"] = $info ;
			$db->updateLoginStatus($_POST['wistron-id'],true);
		}
		
		
		
		echo json_encode($info);
	} else {
		$log->e("Wistron Active Directory is unable to verify the ID [".$_POST['wistron-id']."]");
		echo json_encode(false);
	}*/
	//if you are no on the Wistron Network, Please use the following code to login
	$log->i("Wistron ID [".$_POST['wistron-id']."] is verified. Retrieving permission information from the database server ...");
		$result = $db->getUserInfo("'".$_POST['wistron-id']."'");
		
		$info = array();
		$info["full_name"] = $result[0];	// full name
		$info["email"] = $result[1];	// email
		$info["privilege"] = $result[2];	// privilege
		$info["login"] = true;
		
		if($result[3] == 1){
			$info["suspend"] = true;
			$_SESSION["userInfo"] = null;
		}else{
			$info["suspend"] = false;
			$_SESSION["userInfo"] = $info ;
			
			
		}
		
}else{
	$log->e("Received incomplete credential information");
	echo json_encode(false);
}
?>