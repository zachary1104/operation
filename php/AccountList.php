<?php
include 'DB.php';

$page = isset($_POST['page']) ? $_POST['page'] : 1;
$rp = isset($_POST['rp']) ? $_POST['rp'] : 10;
$sortname = isset($_POST['sortname']) ? $_POST['sortname'] : 'id';
$sortorder = isset($_POST['sortorder']) ? $_POST['sortorder'] : 'desc';
$query = isset($_POST['query']) ? $_POST['query'] : false;
$qtype = isset($_POST['qtype']) ? $_POST['qtype'] : false;

$db = new DB;
$log = new Log;

$sort = "ORDER BY $sortname $sortorder";
$start = (($page-1) * $rp);

$limit = "LIMIT $start, $rp";

$where = "";
//if ($query) $where = " WHERE $qtype LIKE '%".mysql_real_escape_string($query)."%' ";
if ($query) $where = $db->escape_string($qtype,$query);

$sql = "SELECT * FROM o_accounts $where $sort $limit";
$result = $db->execute_sql($sql);

$total = $db->count_records("id","o_accounts $where");
$jsonData = array('page'=>$page,'total'=>$total,'rows'=>array());

while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
     $entry = array('id'=>$row['id'],
		'cell'=>array(
			'id'=>$row['id'],
			'full_name'=>$row['full_name'],
			'email'=>$row['email'],
			'privilege'=>$row['privilege'],
			'online'=>($row['online'] == 1)?'Yes':'No',
			'suspended'=>($row['suspended'] == 1)?'Yes':'No'
		),
	);
	$jsonData['rows'][] = $entry;
}
$db->close();
echo json_encode($jsonData);
?>