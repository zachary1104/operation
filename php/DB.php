<?php
if(!isset($_SESSION)){
    session_start();
}
include "Logger.php";
include "Email.php";
class DB{
	private $conn = null;
	private $err_message = "";
	private $log;
	
	function DB(){
		$this->log = new Log;
		$this->conn = mysqli_connect($_ENV["DB_HOST"],$_ENV["DB_USER"],$_ENV["DB_PASS"],"operation") or die ("Error: could not connect to database");
	}
	
	/*
	 *	Executing the SQL statement
	 *  @return result set;
	 */
	function execute_sql($rsql) {
		$result = null;
		$this->log->i("Executing Query -> ". $rsql);
		if(!($result = $this->conn->query($rsql))){
			$this->log->e("Fetch failed: (" . $this->conn->errno . ") " . $this->conn->error);
			return null;
		}
		return $result;
	}
	
    /*
     *	Counting the total records that retrieve from current query statements
	 *
	 *	@return number of records
     */	 
	function count_records($fname,$tname) {
		$sql = "SELECT count($fname) as 'total' FROM $tname ;";
		if(($result = $this->execute_sql($sql)) == null){
			$this->log->e($this->conn->error);
			return 0;
		};
		$row = $result->fetch_assoc();
		return $row["total"];
	}
	/*
	 *	Escape the string
	 *
	 *  @return formatted query string
	 */
	function escape_string($qtype,$query){
		return " WHERE $qtype LIKE '%".mysqli_real_escape_string($this->conn, $query)."%' ";
	}
	/**
	 *	
	 *
	 */
	function getAssetDocList($id){		
		if(!($this->is_asset_id_exist($id))){
			$this->err_message = "The given Asset id is not exist.";
			return false;
		}
		if(($result = $this->execute_sql("SELECT file_name FROM o_asset_attachments WHERE asset_id = $id")) == null){
			$this->log->e("Unable to retrieve the asset attachment list.");
			return false;
		}
		$aData = array();
		
		while($row = $result->fetch_row()){
			$this->log->d($row[0]);
			array_push($aData,$row[0]);
		}
		return $aData;
	}
	function getCalibrationDocList($id){
		if(!($this->is_asset_id_exist($id))){
			$this->err_message = "The given Asset id is not exist.";
			return false;
		}
		if(($result = $this->execute_sql("SELECT file_name FROM o_calibration_attachments WHERE asset_id = $id")) == null){
			$this->log->e("Unable to retrieve the calibration attachment list.");
			return false;
		}
		$aData = array();
		
		while($row = $result->fetch_row()){
			//$this->log->d($row[0]);
			array_push($aData,$row[0]);
		}
		return $aData;
	}
	function getUserInfo($id){
		
		if(($result = $this->execute_sql("SELECT full_name,email,privilege,suspended FROM o_accounts WHERE UPPER(id) = UPPER(".$id.")")) == null){
			$this->log->e("Unable to update asset information to database.");
			return false;
		}
		return $result->fetch_row();
	}
	function insert_asset($aData){
		$params = "";
		for($i = 0; $i <= 19; $i ++){
			$params .= $aData["id_".$i] . ",";
		}
		$params = ltrim($params,',');
		$params = rtrim($params,',');
		if($this->execute_sql("INSERT INTO o_assetS VALUES(DEFAULT,$params)") == null){
			$this->log->e("Unable to insert asset information to database.");
			return false;
		}
		$asset_id = $this->conn->insert_id;
		
		$this->log->d("Asset Id ". $asset_id);
		
		$license_checked = $aData["id_18"];
		$calibration_checked = $aData["id_19"];
		
		if($license_checked == 1){
			$params = $asset_id . ",";
			for($i = 20; $i <= 26; $i ++){
				$params .= $aData["id_".$i] . ",";
			}
			
			$params = rtrim($params,',');
			if($this->execute_sql("INSERT INTO o_licenses VALUES ($params)") == null){
				$this->log->e("Unable to update asset license information to database. ");
				return false;
			}
			
		}
		
		// If the calibration required is checked, then continue to update the calibration information
		$calibration_id = null;
		if($calibration_checked == 1){
			$params = $asset_id . ",";
			for($i = 27; $i <= 39; $i ++){
				$params .= $aData["id_".$i] . ",";
			}
			$params = rtrim($params,',');
			
			if($this->execute_sql("INSERT INTO o_calibration values(DEFAULT,$params);") == null){
				$this->log->e("Unable to insert the calibration information for Asset ID [$id]");
				return 0;
			}
			$calibration_id = $this->conn->insert_id;
		}
		
		
		$this->logginHistory($asset_id,"Insert a new asset record.");
		$result["asset_id"] = $asset_id;
		$result["calibration_id"] = $calibration_id;
		return $result;
	}
	/*
	 *	Update the asset information
	 *	
	 *	@param $aData  array of asset information data
	 */
	function update_asset($aData){
		$id = $aData['id_0'];
		$params = "";
		
		if(!($this->is_asset_id_exist($id))){
			$this->err_message = "The given Asset id is not exist.";
			return false;
		}
		
		if(($result = $this->execute_sql("SELECT current_assigned_user FROM o_assets WHERE ID =  $id")) == null){
			$this->log->e("Unable to retrieve the current user.");
			return false;
		}
		$row = $result->fetch_assoc(); 
		$currentUser = $row['current_assigned_user'];
		$email_required = false;
		$this->log->d("Verify the notification email. " . $currentUser ." ". $aData["id_13"]);
		if(strtoupper($currentUser) != strtoupper(str_replace("'","",$aData["id_13"]))){
			$email_required = true;
		}
		
		for($i = 0; $i <= 19; $i ++){
			$params .= $aData["id_".$i] . ",";
		}

		$params = rtrim($params,',');
		if($this->execute_sql("CALL p_update_asset_info($params)") == null){
			$this->log->e("Unable to update asset information to database");
			return false;
		}
		$license_checked = $aData["id_18"];
		$calibration_checked = $aData["id_19"];
		
		// If the license required is checked, then continue to update the license information
		if($license_checked == 1){
			$params = $id . ",";
			for($i = 20; $i <= 26; $i ++){
				$params .= $aData["id_".$i] . ",";
			}
			
			$params = rtrim($params,',');
			if($this->contain_license_info($id)){
				if($this->execute_sql("CALL p_update_license_info($params)") == null){
					$this->log->e("Unable to update asset license information to database");
					return false;
				}
			}else{
				if($this->execute_sql("INSERT INTO o_licenses VALUES ($params)") == null){
					$this->log->e("Unable to update asset license information to database.");
					return false;
				}
			}
			
		}
		// If the calibration required is checked, then continue to update the calibration information
		if($calibration_checked == 1){
			$params = $id . ",";
			for($i = 27; $i <= 39; $i ++){
				$params .= $aData["id_".$i] . ",";
			}
			$params = rtrim($params,',');
			
			if($this->contain_calibration_info($id)){
				if($this->execute_sql("CALL p_update_calibration_info($params)") == null){
					$this->log->e("Unable to update asset calibration information to database.");
					return false;
				}
			}else{
				if($this->execute_sql("INSERT INTO o_calibration values(DEFAULT,$params);") == null){
					$this->log->e("Unable to insert the calibration information for Asset ID [$id].");
					return 0;
				}
			}
		}
		
		if($email_required){
			$this->sendNotification($this->getUserInfo("'". $currentUser."'"),$this->getUserInfo($aData["id_13"]),$aData);
			$this->logginHistory($id,"Update the asset information and re-assign the asset to ". $currentUser);
		}else{
			$this->logginHistory($id,"Update the asset information");
		}
		return true;
	}
	function updateAccount($aInfo){
		$query = "UPDATE o_accounts SET ".
				 "full_name = '$aInfo[1]',".
				 "email = '$aInfo[2]',".
				 "privilege = $aInfo[3],".
				 "suspended = $aInfo[4] where id = '$aInfo[0]';";
		if(($result = $this->execute_sql($query)) == null){
			$this->log->e($this->err_message);
			return false;
		}
		return true;
	}
	
	/**
	 *	
	 *	Insert the asset attachments file path
	 *	
	 *	@param int 		$id asset id
	 *	@param string 	$file_path attachment location
	 *	@return boolean true insertion statement successful, otherwise false
	 */
	function insert_asset_attachment($id, $file_path){
		if(!($this->is_asset_id_exist($id))){
			return 0;
		}
		if($this->execute_sql("INSERT INTO asset_attachments VALUES(DEFAULT,$id,$file_path);") == null){
			$this->log->e("Unable insert the asset attachment document.");
			return false;
		}
	}
	/**
	 *	
	 *	Insert the asset attachments file path
	 *	
	 *	@param int 		$id calibration information id
	 *	@param string 	$file_path attachment location
	 *	@return boolean true insertion statement successful, otherwise false
	 */
	function insert_calibration_attachment($id, $file_path){
		if(!($this->is_asset_id_exist($id))){
			return 0;
		}
		if($this->execute_sql("INSERT INTO calibration_attachments VALUES(DEFAULT,$id,$file_path);") == null){
			$this->log->e("Unable insert the calibration attachment document.");
			return false;
		}
	}
	/**
	 *	Verify if the given calibration id is on the current database
	 *	
	 *	@param integer $id
	 *	@return boolean true the given asset id is on the database otherwise false
	 */
	function is_calibration_id_exist($id){
		if(($result = $this->execute_sql("SELECT count(*) as total_count FROM calibration where id = '$id';")) == null){
			$this->log->e("Unable to verify calibration id.");
			return false;
		}
		$row = $result->fetch_assoc(); 
		return $row['total_count'] > 0;
	}
	
	/**
	 *	Verify if the given asset id is on the current database
	 *
	 *	@param integer $id
	 *	@return boolean true the given asset id is on the database otherwise false
	 */
	function is_asset_id_exist($id){
		if(($result = $this->execute_sql("SELECT count(*) as total_count FROM o_assets where id = $id;")) == null){
			$this->log->e("Unable to verify the asset id");
			return false;
		}
		$row = $result->fetch_assoc(); 
		return $row['total_count'] > 0;
	}
	function contain_license_info($id){
		if(($result = $this->execute_sql("SELECT count(*) as total_count FROM o_licenses where id = $id;")) == null){
			$this->log->e("Unable to verify license info.");
			return false;
		}
		$row = $result->fetch_assoc(); 
		return $row['total_count'] > 0;
	}
	function contain_calibration_info($id){
		if(($result = $this->execute_sql("SELECT count(*) as total_count FROM o_calibration where asset_id = $id;")) == null){
			$this->log->e("Unable to verify calibration info.");
			return false;
		}
		$row = $result->fetch_assoc(); 
		return $row['total_count'] > 0;
	}
	/**
	 *	Wistron Tag is unique, so this function is to check if the given Wistron Tag is on the current database
	 *
	 *	@param String $tag
	 *	@return boolean true the given Wistron Tag is on the database otherwise false
	 */
	function is_wistron_tag_exist($tag){
		if(($result = $this->execute_sql("SELECT count(*) as total_count FROM o_assets where wistron_tag = '$tag';")) == null){
			$this->log->e("Unable to verify wistron tag.");
			return false;
		}
		$row = $result->fetch_assoc(); 
		return $row['total_count'] > 0;
	}
	function get_asset_info($id){
		if(($result = $this->execute_sql("CALL p_get_asset_info($id)")) == null){
			$this->log->e("Unable to verify asset id.");
			return false;
		}
		$aData = array();
		$row = $result->fetch_row();
		
		for($i = 0; $i<count($row); $i++){
		    $aData[$i] = $row[$i];
		}
		return $aData;
	}
	function getAssetLogs($id){
		if(!($this->is_asset_id_exist($id))){
			$this->log->e("The given Asset id does not exist.");
			return false;
		}
		if(($result = $this->execute_sql("SELECT event_date,modified_by,email,event FROM o_logs WHERE asset_id = $id ORDER BY id DESC")) == null){
			$this->log->e("Unable to retrieve the asset logs list.");
			return false;
		}
		$aData = array();
		
		while($row = $result->fetch_row()){
			$temp = array();
			$temp["event_time"] = $row[0];
			$temp["modified_by"] = $row[1];
			$temp["email"] = $row[2];
			$temp["event"] = $row[3];
			array_push($aData,$temp);
		}
		return $aData;
	}
	function sendNotification($aOldUser,$aNewUser,$aAsset){
		
		$newUser = "<a href='mailto:$aNewUser[0]/WCH/WISTRON <$aNewUser[1]>'>$aNewUser[0]</a>";
		if(($result = $this->execute_sql("SELECT name FROM o_locations WHERE id = $aAsset[id_16]")) == null){
			$this->log->e("Unable to retrieve the location name.");
			return false;
		}
		$location = $result->fetch_row();
		$message = "The following equipment has been re-assigned to you : $newUser <br /><br />".
				   "Serial Number		:	".$aAsset["id_3"]."<br />".
				   "Manufacturer		:	".$aAsset["id_6"]."<br />".
				   "Model				:	".$aAsset["id_5"]."<br />".
				   "Description			:	".$aAsset["id_7"]."<br />".
				   "Location			:	".$location[0]."<br /><br />".
				   "Please contact the following if this equipment was incorrectly assigned to you <br /><br /><br />".
				   "Non-IT Asset, contact the : <a href='mailto:WCH Operations/WCH/WISTRON <_2193a2@wistron.local>'>Ops Team</a><br />".
				   "IT Asset, contact : <a href='mailto:Brian MacInnis/WCH/WISTRON <Brian_MacInnis@wistron.com>'>Brian MacInnis</a><br /><br /><br />".
				   "Best regards,<br />The Ops Team";
		$subject = "Equipment has been re-assigned to you (Serial Number : ".$aAsset["id_3"]." with Wistron Tag : ".$aAsset["id_1"].")";
		SendEmail($aNewUser[1],$aOldUser[1],$message,$subject);
	}
	
	function updateLoginStatus($eid,$status){
		$this->execute_sql("UPDATE o_accounts SET online = ". (($status)?1:0)." WHERE UPPER(id) = UPPER('$eid')");
	}
	/**
	 *
	 *	Close db connection
	 */
	function close(){
		try{
			mysqli_close($this->conn);
		}catch(Exception $e){
			$this->log->e($e->getMessage());
		}
	}
	/**
	 *	@return string user understandable error message
	 */
	function error_message(){
		return $this->err_message;
	}
	function error(){
		return $this->conn->error;
	}
	function logginHistory($asset_id,$events){
		
		$event_datetime = date('Y-m-d H:i:s');
		$modified_by = $_SESSION["userInfo"]["full_name"];
		$email = $_SESSION["userInfo"]["email"];
		if(($result = $this->execute_sql("INSERT INTO o_logs VALUES(DEFAULT,$asset_id,'".$event_datetime."','".$modified_by."','".$email."','".$events."')")) == null){
			$this->log->e("Unable to verify asset id.");
			return false;
		}
	}
}
?>