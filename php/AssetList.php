<?php
include 'DB.php';
include 'field_table.php';

$log = new Log;
$page = isset($_POST['page']) ? $_POST['page'] : 1;
$rp = isset($_POST['rp']) ? $_POST['rp'] : 10;
$sortname = isset($_POST['sortname']) ? $_POST['sortname'] : 'id';
$sortorder = isset($_POST['sortorder']) ? $_POST['sortorder'] : 'desc';
$query = isset($_POST['query']) ? $_POST['query'] : false;
$qtype = isset($_POST['qtype']) ? $_POST['qtype'] : false;

$log->i("qtype : " . $qtype);
$log->i("query : " . $query);

$db = new DB;


$sort = "ORDER BY $sortname $sortorder";
$start = (($page-1) * $rp);

$limit = "LIMIT $start, $rp";

$where = "";
if($qtype == "filter"){
	$temp = explode("=",$query);
	$filter_type = reset($temp);
	$filter_value = end($temp);
	
	if($filter_type != "reference"){
		
		$where = "where str_to_date($filter_type,'%Y-%m-%d') > CURDATE() ".
				 "AND (DATEDIFF(str_to_date($filter_type,'%Y-%m-%d'),CURDATE()) > ".($filter_value - 30).
				 " AND DATEDIFF(str_to_date($filter_type,'%Y-%m-%d'),CURDATE()) <= ". $filter_value.")";
	}else{
		$lowerBound = ($filter_value == 24)?0 : (($filter_value - 12) / 12) * 365;
		$upperBound = ($filter_value / 12) * 365;
		$where = "WHERE (check_date IS NOT NULL AND reference_interval = $filter_value) AND ".
		         "((DATEDIFF(str_to_date(check_date,'%Y-%m-%d'),CURDATE())* - 1) BETWEEN $lowerBound AND $upperBound)";
	}
}else if($qtype == "custom_filter"){
	$oJson = json_decode($query,true);
	$count = count($oJson);
	$index = 1; 
	$where = " WHERE ";
	foreach ($oJson as $key => $value){
		
		if($index != $count){
			if (strpos($value,"'") !== false) {
				$value = trim($value,"'");
				$where .= "LOWER(" .$field_table[$key]. ") like LOWER('%" . $value . "%') AND ";
			}else{
				$where .= $field_table[$key] . " = " . $value ." AND ";
				
			}
		}else{
			if (strpos($value,"'") !== false) {
				$value = trim($value,"'");
				$where .= "LOWER(" .$field_table[$key]. ") like LOWER('%" . $value . "%') ";
			}else{
				$where .= $field_table[$key] . " = " . $value ." ";
				
			}
		}
		$index++;
	}
	$log->i("WHERE : " . $where);
}else{
	if ($query) $where = $db->escape_string($qtype,$query);
}

$sql = "SELECT * FROM full_asset_view $where $sort $limit";

 
$result = $db->execute_sql($sql);

$total = $db->count_records("id","full_asset_view $where");
$jsonData = array('page'=>$page,'total'=>$total,'rows'=>array());

while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
     $entry = array('id'=>$row['id'],
		'cell'=>array(
			'id'=>$row['id'],
			'wistron_tag'=>$row['wistron_tag'],
			'rim_tag'=>$row['rim_tag'],
			'serial_number'=>$row['serial_number'],
			'status'=>$row['status'],
			'model'=>$row['model'],
			'manufacturer'=>$row['manufacturer'],
			'description'=>$row['description'],
			'received_date'=>$row['received_date'],
			'cost_center'=>$row['cost_center'],
			'po_request_no'=>$row['po_request_no'],
			'po_order_no'=>$row['po_order_no'],
			'expected_life'=>$row['expected_life'],
			'asset_comments'=>$row['asset_comments'],
			'current_assigned_user'=>$row['current_assigned_user'],
			'team'=>$row['team'],
			'current_owner'=>$row['current_owner'],
			'location'=>$row['location'],
			'calibration_required'=>($row['calibration_required'] == 1)?'Yes':'No',
			'maintenance_required'=>($row['maintenance_required']== 1)?'Yes':'No',
		),
	);
	$jsonData['rows'][] = $entry;
}
$db->close();
echo json_encode($jsonData);
?>