<?php
	if(!isset($_SESSION)){
    session_start();
}
	include 'DB.php';

	// Initializing the Logging class
	$log = new Log;
	
	//Verify tow requests parameter 
	if(!isset($_GET['qtype'])){
		$log->e("GET REQUEST parameter 'qtype' is null or empty");
		header("HTTP/1.1 404 GET REQUEST parameter 'qtype' is null or empty", true, 404);
		return;
	}
	if(!isset($_GET['json_data'])){
		$log->e("GET REQUEST parameter 'json_data' is null or empty");
		header("HTTP/1.1 404 GET REQUEST parameter 'json_data' is null or empty", true, 404);
		return;
	}
	$qtype = $_GET['qtype'];
	$json_data = $_GET['json_data'];
	$data = null;
	try{
		$data = json_decode($json_data,true);
	}catch(Exception $e){
		$log->e($e->getMessage());
		echo json_encode(false);
		return;
	}
	$db = new DB; // database object
	
	
	//Data sources queries 
	$aDSQueries = array(
					"accounts" 	=> array("SELECT id, full_name FROM o_accounts ORDER BY full_name;",
										 "unable retrieve account information at this time"),
					"locations" => array("SELECT id, name FROM o_locations ORDER BY name;",
										 "'HTTP/1.1 408 unable retrieve location information at this time'"),
					"status" 	=> array("SELECT id, s_status FROM o_status ORDER BY s_status;",
										 "HTTP/1.1 408 unable retrieve statuses information at this time"),
					"teamList" 	=> array("SELECT id, t_name FROM o_team;",
										 "HTTP/1.1 408 unable retrieve team list information at this time")					 
	);

	switch($qtype){
		case "accounts":
		case "locations":
		case "status":
		case "teamList":
			$aData = array();
			if(($result = $db->execute_sql($aDSQueries[$qtype][0])) == null){
				$log->e($db->error_message());
				header('HTTP/1.1 408 Query failed', true, 408);
				return;
			};
			while($row = $result->fetch_row()){
				$aData[$row[0]] = $row[1];
			}
			echo json_encode($aData);
			return ;
		case "insert_asset":
			$result = $db->insert_asset(json_decode($json_data,true));
			if($result == 0){
				$log->e($db->error_message());
				header('HTTP/1.1 408 Query failed', true, 408);
			}
			echo json_encode($result);
			return;
		case "update_asset":
			$result = $db->update_asset(json_decode($json_data,true));
			if($result == 0){
				$log->e($db->error_message());
				header('HTTP/1.1 408 Query failed', true, 408);
			}
			echo json_encode(true);
			return;
		case "reactivate":
			$log->i("DELETE FROM decommissioned WHERE asset_id = ".$data['id']." AND wistron_tag = ".$data['wistron_tag'].";");
			echo json_encode($db->execute_sql("DELETE FROM decommissioned WHERE asset_id = ".$data['id']." AND wistron_tag = ".$data['wistron_tag'].";"));
			return;
		case "getAssetInfo":
			if(!isset($data['asset_id'])){
				$log->e("The POST request does not contain the asset id.");
				header('HTTP/1.1 408 Query failed', true, 408);
				return;
			}
			echo json_encode($db->get_asset_info($data['asset_id']));
			return;
		case "getAssetDocList":
			if(!isset($data['asset_id'])){
				$log->e("The POST request does not contain the asset id.");
				header('HTTP/1.1 408 Query failed', true, 408);
				return;
			}
			echo json_encode($db->getAssetDocList($data['asset_id']));
			return;
		case "getCalibrationDocList":
			if(!isset($data['asset_id'])){
				$log->e("The POST request does not contain the calibration id.");
				header('HTTP/1.1 408 Query failed', true, 408);
				return;
			}
			echo json_encode($db->getCalibrationDocList($data['asset_id']));
			return;
		case "deleteAttachment":
			if(!isset($data['asset_id'])){
				$log->e("The POST request does not contain the asset id.");
				header('HTTP/1.1 408 Query failed', true, 408);
				return;
			}
			if(!isset($data['file_name'])){
				$log->e("The POST request does not contain the asset id.");
				header('HTTP/1.1 408 Query failed', true, 408);
				return;
			}
			$asset_id = $data['asset_id'];
			$file_name = $data['file_name'];
			$file_category = $data['file_category'];
			if($file_category == 'asset'){
				echo json_encode($db->execute_sql("DELETE FROM o_asset_attachments where asset_id =  $asset_id AND file_name = '$file_name'"));
			}else if($file_category == "calibration"){
				echo json_encode($db->execute_sql("DELETE FROM o_calibration_attachments where asset_id =  $asset_id AND file_name = '$file_name'"));
			}
			return;
		
		case "getCalibrationID":
			if(!isset($data['asset_id'])){
				$log->e("The POST request does not contain the asset id.");
				header('HTTP/1.1 408 Query failed', true, 408);
				return;
			}
			return;
		case "getAssetLogs":
			if(!isset($data['asset_id'])){
				$log->e("The POST request does not contain the asset id.");
				header('HTTP/1.1 408 Query failed', true, 408);
				return;
			}
			echo json_encode($db->getAssetLogs($data['asset_id']));
			return;
		case "insertAssetDocument":
			if(!isset($data['asset_id'])){
				$log->e("The POST request does not contain the asset id.");
				header('HTTP/1.1 408 Query failed', true, 408);
				return;
			}
			if(!isset($data['file_name'])){
				$log->e("The POST request does not contain the file name.");
				header('HTTP/1.1 408 Query failed', true, 408);
				return;
			}
			echo json_encode($db->execute_sql("INSERT INTO o_asset_attachments VALUES(DEFAULT,'".$data['asset_id']."','".$data['file_name']."')"));
			return;
		case "insertCalibrationDocument":
			if(!isset($data['asset_id'])){
				$log->e("The POST request does not contain the calibration id.");
				header('HTTP/1.1 408 Query failed', true, 408);
				return;
			}
			if(!isset($data['file_name'])){
				$log->e("The POST request does not contain the file name.");
				header('HTTP/1.1 408 Query failed', true, 408);
				return;
			}
			echo json_encode($db->execute_sql("INSERT INTO o_calibration_attachments VALUES(DEFAULT,'".$data['asset_id']."','".$data['file_name']."')"));
			return;
		case "updateAccount":
			echo json_encode($db->updateAccount($data));
			return;
		case "insertAccount":
			echo json_encode($db->execute_sql("INSERT INTO o_accounts VALUES('$data[0]','$data[1]','$data[2]',$data[3],0,$data[4])"));
			return;
		case "updateLocation":
			$manageID = ($data[2] == 'NULL')? "NULL" : "'" .  $data[2] . "'";
			echo json_encode($db->execute_sql("UPDATE o_locations SET name = '$data[1]', manager_id =  $manageID WHERE id = $data[0]"));
			return;
		case "insertLocation":
			$manageID = ($data[1] == 'NULL')? "NULL" : "'" .  $data[1] . "'";
			echo json_encode($db->execute_sql("INSERT INTO o_locations values(DEFAULT,'$data[0]',$manageID)"));
			return;
		case "updateStatus":
			echo json_encode($db->execute_sql("UPDATE o_status SET s_status = '$data[1]' WHERE id = $data[0]"));
			return;
		case "insertStatus":
			echo json_encode($db->execute_sql("INSERT INTO o_status values(DEFAULT,'$data[0]')"));
			return;
		case "updateTeam":
			$oJson;
			try{
				$oJson = json_decode($json_data,true);
			}catch(Exception $e){
				$log->e($e->getMessage());
				echo json_encode(false);
				return;
			}
			echo json_encode($db->execute_sql("UPDATE o_team SET t_name = '$data[1]' WHERE id = $data[0]"));
			return;
		case "insertTeam":
			$oJson;
			try{
				$oJson = json_decode($json_data,true);
			}catch(Exception $e){
				$log->e($e->getMessage());
				echo json_encode(false);
				return;
			}
			echo json_encode($db->execute_sql("INSERT INTO o_team values(DEFAULT,'$data[0]')"));
			return;
		case "deleteDatsource":
			$oJson;
			try{
				$oJson = json_decode($json_data,true);
			}catch(Exception $e){
				$log->e($e->getMessage());
				echo json_encode(false);
				return;
			}
			if($oJson[0] != "Accounts"){
				echo json_encode($db->execute_sql("DELETE FROM o_" . strtolower($data[0]) . " WHERE id = ". $data[1]));
			}else{
				echo json_encode($db->execute_sql("DELETE FROM o_" . strtolower($data[0]) . " WHERE id = '". $data[1] . "'"));
			}
			return;
		case "getLocationManager":
			if(!isset($data['location_id'])){
				$log->e("The POST request does not contain the asset id.");
				header('HTTP/1.1 408 Query failed', true, 408);
				return;
			}
			
			$location_id = $data['location_id'];
			if(($result = $db->execute_sql("SELECT o.full_name FROM (SELECT manager_id FROM o_locations WHERE id = $location_id) l LEFT JOIN o_accounts o ON l.manager_id = o.id")) == null){
				$this->err_message = "Unable to retrieve the account information. Error Message : ". $this->conn->error;
				echo json_encode(false);
			}
			echo json_encode($result->fetch_row()[0]);
			return;
		default:
			$log->e("qtype -> $qtype is null or empty");
			header('HTTP/1.1 408 request qtype -> $type not defined.', true, 408);
			return;
	}
	$db->close();
?>