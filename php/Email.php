<?php
require_once('PHPMailer/class.phpmailer.php');


function SendEmail($recipient, $cc, $bodyMessage,$subject){
	$log = new Log();
	$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
	$mail->IsSMTP(); // telling the class to use SMTP

	try {
	  $mail->Host       = "WMXBEMAIL4.wmx.wistron"; // SMTP server
	  //$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
	  $mail->SMTPAuth   = true;                  // enable SMTP authentication
	  $mail->Port       = 25;                    // set the SMTP port for the GMAIL server
	  $mail->Username   = $_ENV["EMAIL_USER"]; 		// SMTP account username
	  $mail->Password   = $_ENV["EMAIL_PASS"];        // SMTP account password
	  $mail->AddAddress($recipient);
	  $mail->AddCC($_ENV["OPERATION_TEAM"]);	//operation team's mail alias 
	  $mail->AddCC($cc);
	  $mail->AddCC($_ENV["IT_EMAIL"]);
	  $mail->SetFrom('WCH_Recal@wistron.com');
	  $mail->Subject = $subject;
	  $mail->MsgHTML($bodyMessage);
	  $mail->Send();

	  $log->i("Re-Assignment Email sent to the ".$recipient);
	} catch (phpmailerException $e) {
	  $log->e($e->errorMessage());
	} catch (Exception $e) {
	  $log->e($e->getMessage());
	}
}
function SendEmailToUsers($equipID,$oldUserID,$aEquip){
	$log = new Log();
	if($oldUserID != $aEquip["currentUser"]){
		$oldUser = getUserAccountInfo($oldUserID);
		$currentUser = getUserAccountInfo($aEquip["currentUser"]);
		$locationDescription = getLocationDescription($aEquip["location"]);
		$message = "The following equipment has been re-assigned to you : ".$currentUser['email']."<br /><br />".
				   "Serial Number		:	".$aEquip["sn"]."<br />".
				   "Manufacturer		:	".$aEquip["manufacturer"]."<br />".
				   "Model				:	".$aEquip["model"]."<br />".
				   "Description			:	".$aEquip["description"]."<br />".
				   "Location			:	".$locationDescription."<br /><br />".
				   "Please contact the following if this equipment was incorrectly assigned to you <br /><br /><br />".
				   "Non-IT Asset, contact the : <a href='mailto:WCH Operations/WCH/WISTRON <_2193a2@wistron.local>'>Ops Team</a><br />".
				   "IT Asset, contact : <a href='mailto:Brian MacInnis/WCH/WISTRON <Brian_MacInnis@wistron.com>'>Brian MacInnis</a><br /><br /><br />".
				   "Best regards,<br />The Ops Team";
		$subject = "Equipment has been re-assigned to you (Serial Number : ".$aEquip["sn"]." with Wistron Tag : ".$aEquip["wistronTag"].")";
		SendEmail($currentUser['email'],$oldUser["email"],$message,$subject);
	}else{
		$log->i("The equipment ".$equipID." assigned user does not change at this time");
	}

}
?>