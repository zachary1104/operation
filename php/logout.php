<?php
	session_start();
	include 'DB.php';
	$log = new Log;
	$db = new DB;
	$db->updateLoginStatus($_SESSION["login_id"],false);
	$log->i($_SESSION["login_id"] . " sign out");
	session_destroy();
	$_SESSION["userInfo"] = null;
	header("location: http://localhost/operation");
?>