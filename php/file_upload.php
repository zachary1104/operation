<?php
	include "Logger.php";
	$log = new Log();
	/*
	ob_start();
	var_dump($_FILES);
	*/
	$result = true;
	//Сheck that  a file
	if((!empty($_FILES["uploaded_file"])) && ($_FILES['uploaded_file']['error'] == 0)) {
	
	  //Check if the file is PDF or DOC image and it's size is less than 350Kb
	  $filename = basename($_FILES['uploaded_file']['name']);
	  $ext = substr($filename, strrpos($filename, '.') + 1);
	
		if (($ext == "pdf" || $ext == "doc") && 
			($_FILES["uploaded_file"]["type"] == "application/pdf" || $_FILES["uploaded_file"]["type"] == "application/msword") && 
			($_FILES["uploaded_file"]["size"] < 10485760)) {
				
			 //Determine the path to which we want to save this file
			 //$newname = dirname(__FILE__).'/attachments/'.$filename;
			 //$newname = '../attachments/asset/'.$filename;
			 $newname = '../attachments/'.$_POST['directory'].'/'.$filename;
			 $log->d($newname);
			if(!file_exists("../attachments/".$_POST['directory'])){
				mkdir("../attachments/".$_POST['directory'],0777,true);
			}

			if ((move_uploaded_file($_FILES['uploaded_file']['tmp_name'],$newname))) {
			   $log->i("It's done! The file has been saved as: ".$newname);
			} else {
			   $log->e("Error: A problem occurred during file upload!");
			   $result = false;
			}
		} else {
			
			 $log->e("Error: Only .pdf .doc images under 350Kb are accepted for upload");
			 $result = false;
		}
	} else {

		$log->e("Error: No file uploaded");
		$result = false;
	}
	echo $result ? "true":"false";
?>