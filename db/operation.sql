-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 10, 2014 at 02:06 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `operation`
--
CREATE DATABASE IF NOT EXISTS `operation` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `operation`;

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURE IF EXISTS `p_get_asset_info`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_get_asset_info`(IN `_id` int)
BEGIN
	SELECT
	a.id,
	a.wistron_tag,
	a.rim_tag,
	a.serial_number,
	a.status,
	a.model,
	a.manufacturer,
	a.description,
	a.received_date,
	a.cost_center,
	a.po_request_no,
	a.po_order_no,
	a.expected_life,
	a.current_assigned_user,
	a.team,
	a.current_owner,
	a.location,
	a.comments,
	a.maintenance_required,
	a.calibration_required,
	l.hw_start_date,
	l.hw_end_date,
	l.hw_cost,
	l.sw_start_date,
	l.sw_end_date,
	l.sw_cost,
	l.comments,
	c.calibration_num,
	c.notification,
	c.reference,
	c.check_date,
	c.reference_interval,
	c.last_date,
	c.next_date_mfg,
	c.next_date_wch,
	c.calibration_interval,
	c.calibration_cost,
	c.contact_end_date,
	c.classification,
	c.comments
	FROM
	(SELECT * FROM o_assets 
		WHERE id = _id) a
  LEFT JOIN o_licenses l ON a.id = l.asset_id
	LEFT JOIN o_calibration c ON a.id = c.asset_id;

END$$

DROP PROCEDURE IF EXISTS `p_insert_asset_info`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_insert_asset_info`(IN `_wistron_tag` VARCHAR(50), IN `_rim_tag` VARCHAR(50), IN `_serial_number` VARCHAR(50), IN `_status` VARCHAR(10), IN `_model` VARCHAR(100), IN `_manufacturer` VARCHAR(100), IN `_description` TEXT, IN `_received_date` DATE, IN `_cost_center` VARCHAR(30), IN `_po_request_no` VARCHAR(50), IN `_po_order_no` VARCHAR(50), IN `_expected_life` INT, IN `_comments` TEXT, IN `_current_user` VARCHAR(10), IN `_team` INT, IN `_owner` VARCHAR(10), IN `_location` INT, IN `_calibration_required` BIT, IN `_maintenance_required` BIT)
BEGIN

INSERT INTO o_assets
VALUES (DEFAULT,
_wistron_tag,
_rim_tag,
_serial_number,
_status,
_model,
_manufacturer,
_description,
_received_date,
_cost_center,
_po_request_no,
_po_order_no,
_expected_life,
_comments,
_current_user,
_team,
_owner,
_location,
_calibration_required,
_maintenance_required
);

END$$

DROP PROCEDURE IF EXISTS `p_insert_calibration_info`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_insert_calibration_info`(IN `_asset_id` INT,  IN `_notification` INT, IN `_last_date` DATE, IN `_next_date` DATE, IN `_calibration_interval` INT, IN `_calibration_cost` DECIMAL, IN `_contact_end_date` DATE, IN `_reference` BIT, IN `_reference_INTerval` INT, IN `_check_date` DATE, IN `_classification` TEXT, OUT `_id` INT)
BEGIN

INSERT INTO calibration VALUES (
DEFAULT,
_asset_id,
_notification,
_last_date,
_next_date,
_calibration_interval,
_calibration_cost,
_contact_end_date,
_reference,
_reference_interval,
_check_date,
_classification);

SELECT LAST_INSERT_ID()
INTO _id;

END$$

DROP PROCEDURE IF EXISTS `p_insert_license_info`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_insert_license_info`(IN `_asset_id` int,IN `_hw_start_date` date,IN `_hw_end_date` date,IN `_hw_cost` decimal,IN `_sw_start_date` date,IN `_sw_end_date` date,IN `_sw_cost` decimal,IN `_comments` text)
INSERT o_licenses
	VALUES(DEFAULT,
			hw_start_date = _hw_start_date,
			hw_end_date = _hw_end_date,
			hw_cost = _hw_cost,
			sw_start_date = _sw_start_date,
			sw_end_date = _sw_end_date,
			sw_cost = _sw_cost,
			comments = _comments
  )$$

DROP PROCEDURE IF EXISTS `p_update_asset_info`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_update_asset_info`(IN `_asset_id` INT, IN `_wistron_tag` VARCHAR(50), IN `_rim_tag` VARCHAR(50), IN `_serial_number` VARCHAR(50), IN `_status` VARCHAR(10), IN `_model` VARCHAR(100), IN `_manufacturer` VARCHAR(100), IN `_description` TEXT, IN `_received_date` DATE, IN `_cost_center` VARCHAR(30), IN `_po_request_no` VARCHAR(50), IN `_po_order_no` VARCHAR(50), IN `_expected_life` INT,  IN `_current_assigned_user` VARCHAR(10),  IN `_team` INT, IN `_current_owner` VARCHAR(10), IN `_location` INT, IN `_comments` TEXT, IN `_maintenance_required` BIT, IN `_calibration_required` BIT)
BEGIN
UPDATE o_assets
SET wistron_tag = _wistron_tag,
rim_tag = _rim_tag,
serial_number = _serial_number,
status = _status,
model = _model,
manufacturer = _manufacturer,
description = _description,
received_date = _received_date,
cost_center = _cost_center,
po_request_no = _po_request_no,
po_order_no = _po_order_no,
expected_life = _expected_life,
comments = _comments,
current_assigned_user = _current_assigned_user,
team = _team,
current_owner = _current_owner,
location = _location,
maintenance_required = _maintenance_required,
calibration_required = _calibration_required
WHERE id = _asset_id;
END$$

DROP PROCEDURE IF EXISTS `p_update_calibration_info`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_update_calibration_info`(IN _asset_id INT, IN `_calibration_num` VARCHAR(15),
IN	_notification BIT,
IN	_reference BIT,
IN	_check_date DATE,
IN	_reference_interval INT,
IN	_last_date DATE,
IN	_next_date_mfg DATE,
IN	_next_date_wch DATE,
IN	_calibration_interval INT, 
IN	_calibration_cost DECIMAL,
IN	_contact_end_date DATE,
IN	_classification TEXT,
IN	_comments TEXT)
BEGIN
   UPDATE o_calibration
	 SET  calibration_num = _calibration_num,
				notification = _notification,
				reference = _reference,
				check_date = _check_date,
				reference_interval = _reference_interval,
				last_date = _last_date,
				next_date_mfg = _next_date_mfg,
				next_date_wch = _next_date_wch,
				calibration_interval = _calibration_interval,
				calibration_cost = _calibration_cost,
				contact_end_date = _contact_end_date,
				classification = _classification,
				comments = _comments
	  WHERE asset_id = _asset_id;
END$$

DROP PROCEDURE IF EXISTS `p_update_license_info`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_update_license_info`(IN `_asset_id` int,IN `_hw_start_date` date,IN `_hw_end_date` date,IN `_hw_cost` decimal,IN `_sw_start_date` date,IN `_sw_end_date` date,IN `_sw_cost` decimal,IN `_comments` text)
BEGIN
	#Routine body goes here...
	UPDATE o_licenses
	SET hw_start_date = _hw_start_date,
			hw_end_date = _hw_end_date,
			hw_cost = _hw_cost,
			sw_start_date = _sw_start_date,
			sw_end_date = _sw_end_date,
			sw_cost = _sw_cost,
			comments = _comments
  WHERE asset_id = _asset_id;

END$$

--
-- Functions
--
DROP FUNCTION IF EXISTS `get_full_name`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_full_name`(_id VARCHAR(10)) RETURNS varchar(255) CHARSET latin1
    DETERMINISTIC
BEGIN
    DECLARE _full_name varchar(255);

		SELECT full_name
		INTO _full_name
		FROM o_accounts
		WHERE	id = _id;
 
    RETURN _full_name;
END$$

DROP FUNCTION IF EXISTS `get_location_name`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_location_name`(_id int) RETURNS varchar(255) CHARSET latin1
    DETERMINISTIC
BEGIN
    DECLARE _location_name varchar(255);

		SELECT name
		INTO _location_name
		FROM o_locations
		WHERE	id = _id;
 
    RETURN _location_name;
END$$

DROP FUNCTION IF EXISTS `get_team_name`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_team_name`(_id int) RETURNS varchar(255) CHARSET latin1
    DETERMINISTIC
BEGIN
    DECLARE _team_name varchar(255);

		SELECT t_name
		INTO _team_name
		FROM o_team
		WHERE	id = _id;
 
    RETURN _team_name;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `full_asset_view`
--
DROP VIEW IF EXISTS `full_asset_view`;
CREATE TABLE IF NOT EXISTS `full_asset_view` (
`id` int(5)
,`wistron_tag` varchar(13)
,`rim_tag` varchar(255)
,`serial_number` varchar(50)
,`status` varchar(17)
,`model` varchar(255)
,`manufacturer` varchar(255)
,`description` text
,`received_date` date
,`cost_center` varchar(255)
,`po_request_no` varchar(50)
,`po_order_no` varchar(50)
,`expected_life` int(2)
,`current_assigned_user` varchar(255)
,`team` varchar(255)
,`current_owner` varchar(255)
,`location` varchar(255)
,`asset_comments` text
,`calibration_required` bit(1)
,`maintenance_required` bit(1)
,`hw_start_date` date
,`hw_end_date` date
,`hw_cost` decimal(10,0)
,`sw_start_date` date
,`sw_end_date` date
,`sw_cost` decimal(10,0)
,`license_comments` text
,`calibration_num` varchar(15)
,`notification` bit(1)
,`reference` bit(1)
,`check_date` date
,`reference_interval` int(3)
,`last_date` date
,`next_date_mfg` date
,`next_date_wch` date
,`calibration_interval` int(3)
,`calibration_cost` decimal(10,0)
,`contact_end_date` date
,`classification` text
,`calibration_comments` text
);
-- --------------------------------------------------------

--
-- Table structure for table `o_accounts`
--

DROP TABLE IF EXISTS `o_accounts`;
CREATE TABLE IF NOT EXISTS `o_accounts` (
  `id` varchar(10) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `privilege` tinyint(1) DEFAULT '1',
  `online` tinyint(1) DEFAULT '0',
  `suspended` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `o_assets`
--

DROP TABLE IF EXISTS `o_assets`;
CREATE TABLE IF NOT EXISTS `o_assets` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `wistron_tag` varchar(13) DEFAULT NULL,
  `rim_tag` varchar(255) DEFAULT NULL,
  `serial_number` varchar(50) DEFAULT NULL,
  `status` varchar(17) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `manufacturer` varchar(255) DEFAULT NULL,
  `description` text,
  `received_date` date DEFAULT NULL,
  `cost_center` varchar(255) DEFAULT NULL,
  `po_request_no` varchar(50) DEFAULT NULL,
  `po_order_no` varchar(50) DEFAULT NULL,
  `expected_life` int(2) DEFAULT NULL,
  `current_assigned_user` varchar(10) DEFAULT NULL,
  `team` int(2) DEFAULT NULL,
  `current_owner` varchar(10) DEFAULT NULL,
  `location` int(2) DEFAULT NULL,
  `comments` text,
  `maintenance_required` bit(1) DEFAULT NULL,
  `calibration_required` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `current_owner` (`current_owner`),
  KEY `current_assigned_user` (`current_assigned_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2454 ;

-- --------------------------------------------------------

--
-- Table structure for table `o_asset_attachments`
--

DROP TABLE IF EXISTS `o_asset_attachments`;
CREATE TABLE IF NOT EXISTS `o_asset_attachments` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `asset_id` int(5) NOT NULL,
  `file_name` text,
  PRIMARY KEY (`id`,`asset_id`),
  KEY `asset_id` (`asset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o_calibration`
--

DROP TABLE IF EXISTS `o_calibration`;
CREATE TABLE IF NOT EXISTS `o_calibration` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `asset_id` int(5) NOT NULL,
  `calibration_num` varchar(15) DEFAULT NULL,
  `notification` bit(1) DEFAULT NULL,
  `reference` bit(1) DEFAULT NULL,
  `check_date` date DEFAULT NULL,
  `reference_interval` int(3) DEFAULT NULL,
  `last_date` date DEFAULT NULL,
  `next_date_mfg` date DEFAULT NULL,
  `next_date_wch` date DEFAULT NULL,
  `calibration_interval` int(3) DEFAULT NULL,
  `calibration_cost` decimal(10,0) DEFAULT NULL,
  `contact_end_date` date DEFAULT NULL,
  `classification` text,
  `comments` text,
  PRIMARY KEY (`id`,`asset_id`),
  KEY `asset_id` (`asset_id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2454 ;

-- --------------------------------------------------------

--
-- Table structure for table `o_calibration_attachments`
--

DROP TABLE IF EXISTS `o_calibration_attachments`;
CREATE TABLE IF NOT EXISTS `o_calibration_attachments` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `asset_id` int(5) NOT NULL,
  `file_name` text,
  PRIMARY KEY (`id`,`asset_id`),
  KEY `asset_id` (`asset_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2129 ;

-- --------------------------------------------------------

--
-- Table structure for table `o_licenses`
--

DROP TABLE IF EXISTS `o_licenses`;
CREATE TABLE IF NOT EXISTS `o_licenses` (
  `asset_id` int(5) NOT NULL,
  `hw_start_date` date DEFAULT NULL,
  `hw_end_date` date DEFAULT NULL,
  `hw_cost` decimal(10,0) DEFAULT NULL,
  `sw_start_date` date DEFAULT NULL,
  `sw_end_date` date DEFAULT NULL,
  `sw_cost` decimal(10,0) DEFAULT NULL,
  `comments` text,
  KEY `asset_id` (`asset_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `o_locations`
--

DROP TABLE IF EXISTS `o_locations`;
CREATE TABLE IF NOT EXISTS `o_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `manager_id` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=258 ;

-- --------------------------------------------------------

--
-- Table structure for table `o_logs`
--

DROP TABLE IF EXISTS `o_logs`;
CREATE TABLE IF NOT EXISTS `o_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL,
  `event_date` datetime DEFAULT NULL,
  `modified_by` varchar(100) NOT NULL,
  `email` text,
  `event` text NOT NULL,
  PRIMARY KEY (`id`,`asset_id`),
  KEY `a_id` (`asset_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- Table structure for table `o_status`
--

DROP TABLE IF EXISTS `o_status`;
CREATE TABLE IF NOT EXISTS `o_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `s_status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `o_team`
--

DROP TABLE IF EXISTS `o_team`;
CREATE TABLE IF NOT EXISTS `o_team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `t_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- Structure for view `full_asset_view`
--
DROP TABLE IF EXISTS `full_asset_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `full_asset_view` AS select distinct `oa`.`id` AS `id`,`oa`.`wistron_tag` AS `wistron_tag`,`oa`.`rim_tag` AS `rim_tag`,`oa`.`serial_number` AS `serial_number`,`oa`.`status` AS `status`,`oa`.`model` AS `model`,`oa`.`manufacturer` AS `manufacturer`,`oa`.`description` AS `description`,`oa`.`received_date` AS `received_date`,`oa`.`cost_center` AS `cost_center`,`oa`.`po_request_no` AS `po_request_no`,`oa`.`po_order_no` AS `po_order_no`,`oa`.`expected_life` AS `expected_life`,`get_full_name`(`oa`.`current_assigned_user`) AS `current_assigned_user`,`get_team_name`(`oa`.`team`) AS `team`,`get_full_name`(`oa`.`current_owner`) AS `current_owner`,`get_location_name`(`oa`.`location`) AS `location`,`oa`.`comments` AS `asset_comments`,`oa`.`calibration_required` AS `calibration_required`,`oa`.`maintenance_required` AS `maintenance_required`,`ol`.`hw_start_date` AS `hw_start_date`,`ol`.`hw_end_date` AS `hw_end_date`,`ol`.`hw_cost` AS `hw_cost`,`ol`.`sw_start_date` AS `sw_start_date`,`ol`.`sw_end_date` AS `sw_end_date`,`ol`.`sw_cost` AS `sw_cost`,`ol`.`comments` AS `license_comments`,`oc`.`calibration_num` AS `calibration_num`,`oc`.`notification` AS `notification`,`oc`.`reference` AS `reference`,`oc`.`check_date` AS `check_date`,`oc`.`reference_interval` AS `reference_interval`,`oc`.`last_date` AS `last_date`,`oc`.`next_date_mfg` AS `next_date_mfg`,`oc`.`next_date_wch` AS `next_date_wch`,`oc`.`calibration_interval` AS `calibration_interval`,`oc`.`calibration_cost` AS `calibration_cost`,`oc`.`contact_end_date` AS `contact_end_date`,`oc`.`classification` AS `classification`,`oc`.`comments` AS `calibration_comments` from ((`o_assets` `oa` left join `o_licenses` `ol` on((`oa`.`id` = `ol`.`asset_id`))) left join `o_calibration` `oc` on((`oa`.`id` = `oc`.`asset_id`)));

--
-- Constraints for dumped tables
--

--
-- Constraints for table `o_assets`
--
ALTER TABLE `o_assets`
  ADD CONSTRAINT `o_assets_ibfk_1` FOREIGN KEY (`current_owner`) REFERENCES `o_accounts` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `o_assets_ibfk_2` FOREIGN KEY (`current_assigned_user`) REFERENCES `o_accounts` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `o_asset_attachments`
--
ALTER TABLE `o_asset_attachments`
  ADD CONSTRAINT `o_asset_attachments_ibfk_1` FOREIGN KEY (`asset_id`) REFERENCES `o_assets` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `o_calibration`
--
ALTER TABLE `o_calibration`
  ADD CONSTRAINT `o_calibration_ibfk_1` FOREIGN KEY (`asset_id`) REFERENCES `o_assets` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `o_calibration_attachments`
--
ALTER TABLE `o_calibration_attachments`
  ADD CONSTRAINT `o_calibration_attachments_ibfk_1` FOREIGN KEY (`asset_id`) REFERENCES `o_assets` (`id`);

--
-- Constraints for table `o_licenses`
--
ALTER TABLE `o_licenses`
  ADD CONSTRAINT `o_licenses_ibfk_1` FOREIGN KEY (`asset_id`) REFERENCES `o_assets` (`id`) ON UPDATE NO ACTION;

--
-- Constraints for table `o_logs`
--
ALTER TABLE `o_logs`
  ADD CONSTRAINT `o_logs_ibfk_1` FOREIGN KEY (`asset_id`) REFERENCES `o_assets` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
