<?php 
	session_start();
	$_SESSION["login_id"] = "";
	if(!isset($_SESSION["userInfo"])){
		$_SESSION["userInfo"] = null;
	}
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" >
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="shortcut icon" type="image/ico" href="images/wistron.ico" />
	<title>::::: Wistron Mobile Solution - Asset Inventory Management :::::</title>
	
	<!-- Styles -->
	<link type="text/css" rel="stylesheet" href="css/asset.css"/>
	<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/> 
	<link type="text/css" rel="stylesheet" href="css/modal.css"/> 
	<link type="text/css" rel="stylesheet" href="css/flexigrid.css"/>
	<link type="text/css" rel="stylesheet" href="css/bootstrap-datetimepicker.min.css" />
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

	<!-- scripts -->
	<script type="text/javascript" src="js/jquery-2.1.1.min.js" ></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<!--<script type="text/javascript" src="js/asset_config.js"></script>-->
	<script type="text/javascript" src="js/modal-min.js"></script>
	<script type="text/javascript" src="js/flexigrid-min.js"></script>
	<script type="text/javascript" src="js/moment.min.js"></script>
	<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
	<script LANGUAGE= "javascript">
		var privilege = 1;
		$( document ).ready(function(){
			if(!<?php echo $_SESSION["userInfo"] == null? "false": "true" ;?>){
				$("#singInModal").modal({backdrop:false});
				$("body").append('<div class="modal-backdrop fade in"></div>');
				$("#signin").on("click",function(event){
					event.preventDefault();
					if($("#wistron-id").val() == ""){
						$("#singInModal #failed-login").append("<p>Please fill out the Wistron ID</p>");
						$(this).attr("disabled","disabled");
						return;
					}
					if($("#password").val() == ""){
						$("#singInModal #failed-login").append("<p>Please fill out the password</p>");
						$(this).attr("disabled","disabled");
						return;
					}
					$(this).attr("disabled","disabled");
					$(this).html("");
					$(this).append('<i class="fa fa-refresh fa-spin"></i>');
					setTimeout(2000);
					console.log("Test");
					$.ajax({
						url:"php/login.php",
						type:'POST',
						async:false,
						data:{"wistron-id":$("#wistron-id").val(),"password":$("#password").val()},
						success: function(data){
							
							var userInfo = jQuery.parseJSON(data);
							//console.log("Login :" + userInfo.login);
							if(userInfo.login == true){
								if(!userInfo.suspend){
									$("#singInModal").modal("hide");
									$(".modal-backdrop").remove();
									var script =  document.createElement("script");
										script.type = "text/javascript";
										script.src = "js/asset_config.js";
									$('body').append(script);
									privilege = userInfo.privilege;
									$("#login-user p").text("WCH / "+userInfo.full_name);
									
								}else{
									$("#singInModal #failed-login").append("<p>Your account is currently suspended. Please contact the administrator for assistence.</p>");
								}
								
							}else{
								$("#singInModal #failed-login").append("<p>Wistron ID and Password does not match.</p>");
								$("#singInModal input").val();
							}
						}
					});
					
				});
				$("#singInModal input").on("click",function(event){
					event.preventDefault();
					$("#singInModal #failed-login > p").remove();
					$("#signin").removeAttr("disabled");
				});
			}else{
				
				$("#login-user p").text("WCH / "+<?php echo json_encode((isset($_SESSION["userInfo"]["full_name"])?$_SESSION["userInfo"]["full_name"]:"Unknown")); ?>);
				privilege = <?php echo (isset($_SESSION["userInfo"]["privilege"])?$_SESSION["userInfo"]["privilege"]:1); ?>;
				if(privilege != 3){
					$(".main_tab_control > ul > li:nth-child(3)").remove();
				}else{
					$("#setting-tab").css("visibility","visible");
				}
				var script =  document.createElement("script");
					script.type = "text/javascript";
					script.src = "js/asset_config.js";
				$('body').append(script);
				
			}
			
		});
	</script>
</head>
<body>
<div id='wrapper'>
	<!-- header -->
	<div id='header'>
		<img id='logo' src='images/WistronLogo.png' height='60' alt=""/>
		<span>Operation Management</span>
		<div id="login-user"><p><?php echo ((!isset($_SESSION["full_name"]))?"": "WCH / " .$_SESSION["full_name"]);?></p></div><br/>
		<a href="http://localhost/operation/php/logout.php" id="logout" title="Log out"><i class="fa fa-sign-out fa-2x"></i></a>
	</div>
	<!-- content -->
	<div id='content'>
		<div class='main_tab_control tab-left'>
			<ul class="nav nav-tabs nav-close" role="tablist" >
			  <li id="nav-icon"><span id='toggle-icon'><i class="fa fa-caret-square-o-right "></i></span></li>
			  <li class="active"><a href="#asset_table_content" role="tab" data-toggle="tab">Asset Table</a></li>
			  <li id="setting-tab"><a role="tab" data-toggle="tab">Settings</a></li>
			</ul>
			<div class="tab-content">
			<!--      Asset table tab -->
			  <div class="tab-pane active" id="asset_table_content">
					<div id='asset_table_wrapper'>
						<table id="asset_table" style="display:none"></table>
					</div>
			  </div>
			  <div class="tab-pane" id="settings_table_content">
				<fieldset id="setting-button-wrapper">
					 <legend>System Data Source</legend>
					 <button type="button" class="btn btn-primary">Accounts</button>
					 <button type="button" class="btn btn-primary">Location</button>
					 <button type="button" class="btn btn-primary">Status</button>
					 <button type="button" class="btn btn-primary">Team information</button>
				</fieldset>
			  </div>
			</div>
		</div>
	</div>
	<!-- footer -->
	<div id='footer'><span id="copyright">Power by <a href="mailto:zachary_huang@wistron.com">Zachary Huang</a> @ Wistron Mobile Solution Corporation</span></div>
</div>

<!--  templates  -->

<script type="template/text" id="window_template">
	<div class="window">
		<!-- Modal Window header -->
		<div class="window-header">
			<h4 class="window-title"></h4>
		</div>
		<!-- Modal window boday -->
		<div class="window-body"></div>
		<div class="window-footer"></div>
	</div> <!-- Window -->
</script>

<script type='template/text' id='asset_editor_template'>
<!-- Nav tabs -->
<div class='asset_editor_tab_control tab-left'>
	<ul class="nav nav-tabs" role="tablist">
	  <li class="active"><a href="#basic_info" role="tab" data-toggle="tab">Basic info<span class="badge hide">0</span></a></li>
	  <li><a href="#history" role="tab" data-toggle="tab">History</a></li>
	  <li><a href="#license_info" role="tab" data-toggle="tab" >License<span class="badge hide"></span></a></li>
	  <li><a href="#calibration_info" role="tab" data-toggle="tab">Calibration<span class="badge hide"></span></a></li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<div class="tab-pane active" id="basic_info">
			<div class="left">
				<fieldset>
					<div class="form-group">
						<label class="control-label" for="id_1">Wistron Tag :<span class="require_field">*</span></label>
						<input id="id_1" type="text" class="form-control input-sm" data-type="string" data-placement="right" data-content="1. Require Field. 2. Max length : 13" >
					</div>
					<div class="form-group">
						<label class="control-label" for="id_2">RIM Tag : </label>
						<input id="id_2" class="form-control input-sm" type="text" data-type="string" data-typ='string' data-placement="right" data-content="Max length : 30">
					</div>
					<div class="form-group">
						<label class="control-label" for="id_3">Serial # :<span class="require_field">*</span></label>
						<input id="id_3" class="form-control input-sm" type="text" data-type="string" data-placement="right" data-content="1. Require Field. 2. Max length : 50">
					</div>
					<div class="form-group">
						<label class="control-label" for="4">Status : </label>
						<select id="id_4" class="form-control input-sm" data-type="selection"></select>
					</div>
					<div class="form-group">
						<label class="control-label" for="id_5">Model :<span class="require_field">*</span></label>
						<input id="id_5" class="form-control input-sm" type="text" data-type="string" data-placement="right" data-content="1. Require Field. 2. Max length : 255">
					</div>
					<div class="form-group">
						<label class="control-label" for="id_6">Manufacturer: <span class="require_field">*</span></label>
						<input id="id_6" class="form-control input-sm" type="text" data-type="string" data-placement="right" data-content="1. Require Field. 2. Max length : 255">
					</div>
					<div class="form-group">
						<label class="control-label" for="id_7">Description : <span class="require_field">*</span></label>
						<input id="id_7" class="form-control input-sm" type="text" data-type="string" data-placement="right" data-content="1. Require Field. 2. Max length : 255+">
					</div>
					<div class="form-group">
						<label class="control-label" for="id_8">Date Received : </label>
						<div class='input-group date date_fileds' id='received_date' >
							<input id="id_8" class="form-control input-sm " type="text" data-type="date" placeholder="ex. YYYY-MM-DD" data-date-format="YYYY-MM-DD">
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label" for="id_9">Cost Center : <span class="require_field">*</span></label>
						<input id="id_9" class="form-control input-sm" type="text" data-type="string" data-placement="right" data-content="1. Require Field. 2. Max length : 13">
					</div>
				</fieldset>
			</div>
			<div class='right'>
				<fieldset>
					<div class="form-group">
					<label class="control-label" for="id_10">P.R. #: <span class="require_field">*</span></label>
					<input id="id_10" class="form-control input-sm" type="text" data-type="string" data-placement="left" data-content="1. Require Field. 2. Max length : 20">
					</div>
					<div class="form-group">
						<label class="control-label" for="id_11">P.O. #: <span class="require_field">*</span></label>
						<input id="id_11" class="form-control input-sm" type="text" data-type="string" data-placement="left" data-content="1. Require Field. 2. Max length : 20">
					</div>
					<div class="form-group">
						<label class="control-label" for="id_12">Expected Life (Month): </label>
						<input id="id_12" class="form-control input-sm" type="text" data-type="integer" data-placement="left" data-content="Only number (0-9) allow">
					</div>
					<div class="form-group">
						<label class="control-label" for="id_13">Current Assigned User : <span class="require_field">*</span></label>
						<select id="id_13" class="form-control input-sm" data-type="selection" ></select>
					</div>
					<div class="form-group">
						<label class="control-label" for="id_14">Team : <span class="require_field">*</span></label>
						<select id="id_14" class="form-control input-sm" data-type="selection" ></select>
					</div>
					<div class="form-group">
						<label class="control-label" for="id_15">Owner : <span class="require_field">*</span></label>
						<select id="id_15" class="form-control input-sm" data-type="selection" ></select>
					</div>
					<div class="form-group">
						<label class="control-label" for="id_16">Location : <span class="require_field">*</span></label>
						<select id="id_16" class="form-control input-sm" data-type="selection"></select>
					</div>
					<div class="form-group">
						<label class="control-label" for="id_17">comments: </label>
						<textarea id="id_17" class="form-control" rows="2" data-type="string" data-placement="left" data-content="Max length : 255+"></textarea>
					</div>
					<div class="checkbox">
						<label>
						<input id="id_18" type="checkbox" value="">License/Maintenance Required
						</label>
					</div>
					<div class="checkbox">
						<label>
						<input id="id_19" type="checkbox" data-type="checkbox">Calibration Required
						</label>
					</div>
					<button type="button" id='btn_asset_attachments' class="btn btn-primary" disabled>
						<span class="glyphicon  glyphicon-folder-open"></span>Attachments
					</button>
				</fieldset>
			</div>
		</div>
	  <div class="tab-pane" id="history">
		<div id="history-container">
		</div>
	  </div>
	  <div class="tab-pane" id="license_info">
		<div class="left">
			<fieldset>
				<div class="form-group">
					<label class="control-label" for="id_20">Hardware Start Date : </label>
					<div class='input-group date date_fileds' id='hw-start-date' >
						<input id="id_20" class="form-control input-sm" type="text" data-type="date" data-date-format="YYYY-MM-DD" placeholder="ex. YYYY-MM-DD">
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label" for="id_21">Hardware End Date : </label>
					<div class='input-group date date_fileds' id='hw-end-date' >
						<input id="id_21" class="form-control input-sm" type="text" data-type="date" data-date-format="YYYY-MM-DD" placeholder="ex. YYYY-MM-DD">
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label" for="id_22">Hardware Cost : </label>
					<input id="id_22" class="form-control input-sm" type="text" data-type="integer" data-placement="top" data-content="Only number (0-9) allow">
				</div>
				<div class="form-group">
					<label class="control-label" for="id_23">Software Start Date : </label>
					<div class='input-group date date_fileds' id='sw-start-date' >
						<input id="id_23" class="form-control input-sm" type="text" data-type="date" data-date-format="YYYY-MM-DD" placeholder="ex. YYYY-MM-DD">
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label" for="id_24">Software End Date : </label>
					<div class='input-group date date_fileds' id='sw-end-date'>
						<input id="id_24" class="form-control input-sm" type="text" data-type="date" data-date-format="YYYY-MM-DD" placeholder="ex. YYYY-MM-DD">
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
					</div>
				</div>
			</fieldset>
			</div>
			<div class="right">
			<fieldset>
				<div class="form-group">
					<label class="control-label" for="id_25">Software Cost : </label>
					<input id="id_25" class="form-control input-sm" type="text" data-type="integer" data-placement="top" data-content="Only number (0-9) allow">
				</div>
				<div class="form-group">
					<label class="control-label" for="id_26">Comments : </label>
					<textarea id="id_26" class="form-control" rows="4" data-type="string" data-placement="top" data-content="Max length : 255+"></textarea>
				</div>
			</fieldset>
			</div>
	  </div>
	  <div class="tab-pane" id="calibration_info">
		<div class="left">
				<fieldset>
					<div class="form-group">
						<label class="control-label" for="id_27">Calibration #: </label>
						<input id="id_27" class="form-control input-sm" type="text" data-type="string">
					</div>
					<div class="checkbox">
						<label>
						<input id="id_28" type="checkbox" data-type="string">Notification Required
						</label>
					</div>
					<div class="checkbox">
						<label>
						<input id="id_29" type="checkbox" data-type="string">Calibration Reference
						</label>
					</div>
					<div class="form-group">
						<label class="control-label" for="id_30">Reference Check Date: </label>
						<input id="id_30" class="form-control input-sm" type="text" data-type="string" readonly>
					</div>
					<div class="form-group">
						<label class="control-label" for="id_31">Reference Interval (Month): </label>
						<input id="id_31" class="form-control input-sm" type="text" data-type="integer" data-placement="top" data-content="1. Only number (0-9) allow. 2. Max length = 2">
					</div>

					<div class="form-group">
						<label class="control-label" for="id_32">Last Calibration Date: </label>
						<div class='input-group date date_fileds' id='last-cal-date'>
							<input id="id_32" class="form-control input-sm" type="text" data-type="string" data-date-format="YYYY-MM-DD" placeholder="ex. YYYY-MM-DD">
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label" for="id_33">Next calibration date (mfg): </label>
						<div class='input-group date date_fileds' id='next-cal-date'>
							<input id="id_33" class="form-control input-sm" type="text" data-type="string" data-date-format="YYYY-MM-DD" placeholder="ex. YYYY-MM-DD">
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label" for="id_34">Next calibration date (WCH): </label>
						<div class='input-group date date_fileds' id='next-cal-date'>
							<input id="id_34" class="form-control input-sm" type="text" data-type="string" data-date-format="YYYY-MM-DD" placeholder="ex. YYYY-MM-DD">
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
						</div>
					</div>
					

				</fieldset>
			</div>
			<div class='right'>
				<fieldset>
				<div class="form-group">
					<label class="control-label" for="id_35">Calibration Interval (Month): </label>
					<input id="id_35" class="form-control input-sm" type="text" data-type="integer" data-placement="top" data-content="1. Only number (0-9) allow. 2. Max length = 2">
				</div>
				<div class="form-group">
					<label class="control-label" for="id_36">Calibration Cost: </label>
					<input id="id_36" class="form-control input-sm" type="text" data-type="integer" data-placement="top" data-content="Only number (0-9) allow">
				</div>
				<div class="form-group">
					<label class="control-label" for="id_37">Mfr Contract End Date: </label>
					<div class='input-group date date_fileds' id='mfr-contract-end-date'>
						<input id="id_37" class="form-control input-sm" type="text" data-type="string" data-date-format="YYYY-MM-DD" placeholder="ex. YYYY-MM-DD">
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label" for="id_38">Classification: </label>
					<input id="id_38" class="form-control input-sm" type="text" data-type="string" data-placement="top" data-content="Max length : 255">
				</div>
				<div class="form-group">
					<label class="control-label" for="id_39">Comments : </label>
					<textarea id="id_39" class="form-control" rows="4" data-type="string" data-placement="top" data-content="Max length : 255+"></textarea>
				</div>
				<button type="button" id="btn_calibration_attachment" class="btn btn-primary" disabled>
					<span class="glyphicon  glyphicon-folder-open"></span>Calibration Attachments
				</button>
				</fieldset>
			</div>
	  </div>
	</div>
</div>
</script>
<script type='template/text' id='attachments_template'>
<div class='attachment_container'>
	<div class="form-group">
		<input type="file" name="uploaded_file" id='uploaded_file' enctype="multipart/form-data">
	</div>
	<button type="button" id='btn-start-upload' class="btn btn-primary" disabled>
		<span class="glyphicon glyphicon-upload"></span>Upload
	</button>
	
	
	<table class="table" id="attachment-list">
		<thead>
			<tr>
				<th>File Name</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>

</div>
</script>
<script type='template/text' id='filter_template'>
	<div class="panel-group filters" id="accordion">
		<div class="panel panel-default">
			<div class="panel-heading panel-heading-color">
				 <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#incoming-calibration-mfg" class="collapsed">Incoming Calibration (mfg) within</a></h4>
			</div>
			<div id="incoming-calibration-mfg" class="panel-collapse collapse filter-panel">
				<table><tr>
					<td><button type="button" class="btn btn-primary btn-sm filter-buttons" data-filter-type="next_date_mfg">30 days</button></td>
					<td><button type="button" class="btn btn-primary btn-sm filter-buttons" data-filter-type="next_date_mfg">60 days</button></td>
					<td><button type="button" class="btn btn-primary btn-sm filter-buttons" data-filter-type="next_date_mfg">90 days</button></td>
				</tr></table>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading panel-heading-color">
				 <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#incoming-calibration-wch" class="collapsed">Incoming Calibration (WCH) within</a></h4>
			</div>
			<div id="incoming-calibration-wch" class="panel-collapse collapse filter-panel">
				<table><tr>
					<td><button type="button" class="btn btn-primary btn-sm filter-buttons" data-filter-type="next_date_wch">30 days</button></td>
					<td><button type="button" class="btn btn-primary btn-sm filter-buttons" data-filter-type="next_date_wch">60 days</button></td>
					<td><button type="button" class="btn btn-primary btn-sm filter-buttons" data-filter-type="next_date_wch">90 days</button></td>
				</tr></table>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading panel-heading-color">
				 <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#incoming-hw-service" class="collapsed">Incoming Hardware Service within</a></h4>
			</div>
			<div id="incoming-hw-service" class="panel-collapse collapse filter-panel">
				<table><tr>
					<td><button type="button" class="btn btn-primary btn-sm filter-buttons" data-filter-type="hw_end_date">30 days</button></td>
					<td><button type="button" class="btn btn-primary btn-sm filter-buttons" data-filter-type="hw_end_date">60 days</button></td>
					<td><button type="button" class="btn btn-primary btn-sm filter-buttons" data-filter-type="hw_end_date">90 days</button></td>
				</tr></table>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading panel-heading-color">
				 <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#incoming-sw" class="collapsed">Incoming Software License within</a></h4>
			</div>
			<div id="incoming-sw" class="panel-collapse collapse filter-panel">
				<table><tr>
					<td><button type="button" class="btn btn-primary btn-sm filter-buttons" data-filter-type="sw_end_date">30 days</button></td>
					<td><button type="button" class="btn btn-primary btn-sm filter-buttons" data-filter-type="sw_end_date">60 days</button></td>
					<td><button type="button" class="btn btn-primary btn-sm filter-buttons" data-filter-type="sw_end_date">90 days</button></td>
				</tr></table>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading panel-heading-color">
				 <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#reference-checked" class="collapsed">Reference Checked within</a></h4>
			</div>
			<div id="reference-checked" class="panel-collapse collapse filter-panel">
				<table><tr>
					<td><button type="button" class="btn btn-primary btn-sm filter-buttons" data-filter-type="reference">24 month</button></td>
					<td><button type="button" class="btn btn-primary btn-sm filter-buttons" data-filter-type="reference">36 month</button></td>
					<td><button type="button" class="btn btn-primary btn-sm filter-buttons" data-filter-type="reference">48 month</button></td>
					<td><button type="button" class="btn btn-primary btn-sm filter-buttons" data-filter-type="reference">60 month</button></td>
				</tr></table>
			</div>
		</div>
	</div>
</script>
<script type='template/text' id='account_template'>
	<div id="account-editor">
		<div class="form-group">
			<label class="control-label" for="e-id">Employee ID : </label>
			<input id="e-id" class="form-control input-sm" type="text" data-type="integer" data-placement="top" data-content="Only number (0-9) allow">
		</div>
		<div class="form-group">
			<label class="control-label" for="e-fullname">Full Name : </label>
			<input id="e-fullname" class="form-control input-sm" type="text" data-type="integer" data-placement="top" data-content="Only number (0-9) allow">
		</div>
		<div class="form-group">
			<label class="control-label" for="e-email">Email : </label>
			<input id="e-email" class="form-control input-sm" type="text" data-type="integer" data-placement="top" data-content="Only number (0-9) allow">
		</div>
		<h4>privilege : </h4>
		<div id="privilege-group" class="btn-group btn-toggle" data-toggle="buttons">
			<label id='option1' class="btn btn-primary active" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Only allow to view the asset information"><input type="radio" name="options" value="1" >Read</label>
			<label id='option2' class="btn btn-default" data-placement="top" data-content="Allow to view, modify and create asset information"><input type="radio" name="options" value="2">Read & Write</label>
			<label id='option3' class="btn btn-default" data-placement="top" data-content="Full permission with asset + permission to access to the setting"><input type="radio" name="options" value="3">Full Privilege</label>
		</div>
		<h4>Suspended</h4>
		<div id="suspended-group" class="btn-group btn-toggle" data-toggle="buttons">
			<label id='option_yes' class="btn btn-default"><input type="radio" name="options" value="yes">YES</label>
			<label id='option_no' class="btn btn-primary active"><input type="radio" name="options" value="no">NO</label>
			
		</div>
	</div>
</script>
<script type='template/text' id='export_template'>
	<div id='export-fields-container' class='checkbox'>
		
		<div>
		<label><input type="checkbox" id='select-all'>Select ALL</label>
		<button type="button" id='btn-export' class="btn btn-primary">Export to CSV</button>
		</div>
		<table>
			<tbody class="row-group">
			<tr>
				<td><label><input type="checkbox">Asset ID</label></td>
				<td><label><input type="checkbox">Wistron Tag</label></td>
				<td><label><input type="checkbox">RIM Tag</label></td>
				<td><label><input type="checkbox">Serial Number</label></td>
				
			</tr>
			<tr>
				<td><label><input type="checkbox">Status</label></td>
				<td><label><input type="checkbox">Model</label></td>
				<td><label><input type="checkbox">Manufacturer</label></td>
				<td><label><input type="checkbox">Description</label></td>
			</tr>
			<tr>
				<td><label><input type="checkbox">Received Date</label></td>
				<td><label><input type="checkbox">PR</label></td>
				<td><label><input type="checkbox">PO</label></td>
				<td><label><input type="checkbox">Expected Life</label></td>
			</tr>
			<tr>
				<td><label><input type="checkbox">User</label></td>
				<td><label><input type="checkbox">Team</label></td>
				<td><label><input type="checkbox">Owner</label></td>
				<td><label><input type="checkbox">Location</label></td>
			</tr>
			<tr>
				<td><label><input type="checkbox">License Required</label></td>
				<td><label><input type="checkbox">Calibration Required</label></td>
				<td><label><input type="checkbox">Comments</label></td>
			</tr>
			</tbody>
			<tbody class="row-group">
			<tr>
				<td><label><input type="checkbox">HW Start Date</label></td>
				<td><label><input type="checkbox">SW Start Date</label></td>
				<td><label><input type="checkbox">License comments</label></td>
			</tr>
			
			<tr>
				<td><label><input type="checkbox">HW End Date</label></td>
				<td><label><input type="checkbox">SW End Date</label></td>
				
			</tr>
			<tr>
				<td><label><input type="checkbox">HW Cost</label></td>
				<td><label><input type="checkbox">SW Cost</label></td>
				
			</tr>
			
			</tbody>
			<tbody class="row-group">
			<tr>
				<td><label><input type="checkbox">Calibration Number</label></td>
				<td><label><input type="checkbox">Notification Required</label></td>
				<td><label><input type="checkbox">Reference Checked</label></td>
				<td><label><input type="checkbox">Checked Date</label></td>
			</tr>
			<tr>
				
				<td><label><input type="checkbox">Reference Interval</label></td>
				<td><label><input type="checkbox">Calibration Interval</label></td>
				<td><label><input type="checkbox">Calibration Cost</label></td>
				<td><label><input type="checkbox">Contract End Date</label></td>
			</tr>
			<tr>
				
				<td><label><input type="checkbox">Classification</label></td>
				<td><label><input type="checkbox">Last Calibration Date</label></td>
				<td><label><input type="checkbox">Next Calibration Date (mfg)</label></td>
				<td><label><input type="checkbox">Next Calibration Date (WCH)</label></td>
			</tr>
			<tr>
				<td><label><input type="checkbox">Calibration comments</label></td>
			</tr>
			</tbody>
		</table>
	</div>
</script>
<!-- Modal -->
<div class="modal fade bs-modal-sm" id="singInModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
			<h2>Sign In </h2>
		</div>
		<div class="modal-body">
			<div id="failed-login"></div>
			<input type="text" id="wistron-id"class="form-control" placeholder="Wistron ID Ex : chxxxxxxx" required autofocus>
			<input type="password" id="password" class="form-control" placeholder="Password" required><br />
			<button class="btn btn-lg btn-primary btn-block has-spinner" type="submit" id="signin">Sign in</button>
		</div>
		
    </div>
  </div>
</div>
</body>
</html>